$(window).load(function() {
    $('.news article img').captionjs({
        'class_name'      : 'captionjs', // Class name for each <figure>
        'schema'          : true,        // Use schema.org markup (i.e., itemtype, itemprop)
        'mode'            : 'default',   // default | stacked | animated | hide
        'debug_mode'      : false,       // Output debug info to the JS console
        'force_dimensions': true,        // Force the dimensions in case they cannot be detected (e.g., image is not yet painted to viewport)
        'is_responsive'   : true,       // Ensure the figure and image change size when in responsive layout. Requires a container to control responsiveness!
        'inherit_styles'  : false        // Have the caption.js container inherit box-model properties from the original image
    });
   
$('img.right').parent('figure').addClass('figure-right');
$('img.left').parent('figure').addClass('figure-left');
});