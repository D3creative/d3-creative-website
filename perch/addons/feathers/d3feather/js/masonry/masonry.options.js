$(document).ready(function() {
$('.masonry-container').imagesLoaded( function() {
  // init Masonry after all images have loaded
   $('.masonry-container').masonry({
    itemSelector: '.masonry-item',
    columnWidth: '.masonry-item-sizer',
    percentPosition: true,
    gutter: '.masonry-item-gutter-sizer',                    
  });
});
});