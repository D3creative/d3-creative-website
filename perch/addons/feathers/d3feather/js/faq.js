$("#FAQs .answer").hide();
$("#FAQs .close").hide();
$("#FAQs .open").show();

// Click the #Expand button to show and hide .answer
$('#Expand').click(function() {
 
    if ($(this).text() === 'Minimise Facts') {
        $(this).text('Expand Facts');
        $('#FAQs .answer').slideUp('fast');
        $("#FAQs .close").hide();
        $("#FAQs .open").show();

    } else {
        $(this).text('Minimise Facts');
        $('#FAQs .answer').slideDown('fast');
        $("#FAQs .close").show();
        $("#FAQs .open").hide();

    }
});

// Click the Quesiton (h3) to show and hide the answer
$("#FAQs h3").addClass("link").click(function() {
    $(this).parent().children(".answer").slideToggle('fast');
    $(this).parent('li').children(".close").toggle();
    $(this).parent('li').children(".open").toggle();
});
