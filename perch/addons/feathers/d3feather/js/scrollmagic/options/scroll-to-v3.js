$( document ).ready(function() {
        console.log( "document loaded" );
    });

//  If window is over 768 (onload) do this. Doesn't work when resizing, only on reload.
if ($(window).width() > 768) {

// Third version, everything needed to make element stik and highlight anchor is in this script.

// Docs:
// https://github.com/janpaepke/ScrollMagic/wiki/Tutorial-:-Anchor-Navigation
// http://codepen.io/grayghostvisuals/pen/EtdwL
// Init controller

// Scene Handler
var controller = new ScrollMagic.Controller({
  globalSceneOptions: {
     // duration: $('section').height(),
    reverse: true
  }
});


var scene0 = new ScrollMagic.Scene
                ({triggerElement: "#pinned-trigger1", triggerHook: 0})
                .setPin("#pinned-element1") // the element we want to pin
                .addTo(controller);



var scene1 = new ScrollMagic.Scene
                ({ triggerElement: '#section-1', duration: $('#section-1').height(), triggerHook: .095})
                .setClassToggle('#anchor-1', 'active')
                .addTo(controller);

var scene2 = new ScrollMagic.Scene
                ({ triggerElement: '#section-2', duration: $('section').height(), triggerHook: .095})
                .setClassToggle('#anchor-2', 'active')
                .addTo(controller);

var scene3 = new ScrollMagic.Scene
                ({ triggerElement: '#section-3', duration: $('#section-3').height(), triggerHook: .095})
                .setClassToggle('#anchor-3', 'active')
                .addTo(controller);

var scene4 = new ScrollMagic.Scene
                ({ triggerElement: '#section-4', duration: $('#section-4').height(), triggerHook: .095})
                .setClassToggle('#anchor-4', 'active')
                .addTo(controller);

// var scene5 = new ScrollMagic.Scene
//                 ({ triggerElement: '#section-5', duration: $('section').height(), triggerHook: 0.15})
//                 .setClassToggle('#anchor-5', 'active')
//                 .addTo(controller);


// Change behaviour of controller
// to animate scroll instead of jump
controller.scrollTo(function(target) {

  TweenMax.to(window, 0.5, {
    scrollTo : {
      y : target-78, //Height of sub navigation - or the pinned element
      autoKill : true // Allow scroll position to change outside itself
    },
    ease : Cubic.easeInOut
  });

});


//  Bind scroll to anchor links
$(document).on("click", "a[href^=#]", function(e) {
  var id = $(this).attr("href");

  if($(id).length > 0) {
    e.preventDefault();

    // trigger scroll
    controller.scrollTo(id);

    // If supported by the browser we can also update the URL
    if (window.history && window.history.pushState) {
      history.pushState("", document.title, id);
    }
  }

});

} else {
    
}
