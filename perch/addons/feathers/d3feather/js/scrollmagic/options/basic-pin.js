//  If window is over 768 (onload) do this. Doesn't work when resizing, only on reload.
if ($(window).width() > 768) {
// Use triggerElement to trigger the pin and add #pinned-element1 to the element you want to pin.
// init ScrollMagic Controller
var controller = new ScrollMagic.Controller();

// Scene Handler
var scene1 = new ScrollMagic.Scene({
  triggerElement: "#pinned-trigger1", // point of execution
  triggerHook: 0, // don't trigger until #pinned-trigger1 hits the top of the viewport
  reverse: true // allows the effect to trigger when scrolled in the reverse direction
})
.setPin("#pinned-element1") // the element we want to pin
.addTo(controller);
} else {
    
}
