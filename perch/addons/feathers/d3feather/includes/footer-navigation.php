 <footer class="row">
 <div class="inner-container">

 			<div class="pad"><?php perch_pages_navigation(array(
 			             'hide-extensions'      => true,
 			             'template' => 'item-inline.html'
 			             ));?>
 			             <ul class="inline extras">
 			             	<li><a href="http://sheffieldcityregion.org.uk" title="sheffieldcityregion.org.uk">Visit sheffieldcityregion.org.uk</a></li>
 			             	<li><a href="https://twitter.com/SheffCityRegion" title="Follow @SheffCityRegion">Follow @SheffCityRegion</a></li>
 			             </ul>
 			
 			             <p>Copyright © 2012 - <?php echo date('Y'); ?> Sheffield City Region Executive Team. All Rights Reserved. <a href="/legal-disclaimer" title="Legal Disclaimer">Legal Disclaimer</a></p></div>
 </div>
 </footer>