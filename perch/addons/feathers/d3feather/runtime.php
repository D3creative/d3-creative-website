<?php
	
PerchSystem::register_feather('d3feather');

class PerchFeather_d3feather extends PerchFeather
{
	public function get_css($opts, $index, $count)
	{	
		$out = array();

		// if (!$this->component_registered('normalize')) {
		// 	$out[] = $this->_single_tag('link', array(
		// 			'rel'=>'stylesheet',
		// 			'href'=>$this->path.'/css/normalize.css',
		// 			'type'=>'text/css'
		// 		));
		// 	$this->register_component('normalize');
		// }


		if (!$this->component_registered('styles')) {
			$out[] = $this->_single_tag('link', array(
					'rel'=>'stylesheet',
					'href'=>$this->path.'/css/styles.css',
					'type'=>'text/css'
				));
			$this->register_component('styles');
		}

		// if (!$this->component_registered('font-awesome')) {
		// 	$out[] = $this->_single_tag('link', array(
		// 			'rel'=>'stylesheet',
		// 			'href'=>$this->path.'/css/font-awesome/font-awesome.min.css',
		// 			'type'=>'text/css'
		// 		));
		// 	$this->register_component('font-awesome');
		// }

		// if (!$this->component_registered('frutiger')) {
		// 	$out[] = $this->_single_tag('link', array(
		// 			'rel'=>'stylesheet',
		// 			'href'=>$this->path.'/css/frutiger/frutiger.min.css',
		// 			'type'=>'text/css'
		// 		));
		// 	$this->register_component('frutiger');
		// }

		

		
		return implode("\n\t", $out)."\n";
	}

	public function get_javascript($opts, $index, $count)
	{
		$out = array();

		// Jquery is combined with flaunt-min
		// if (!$this->component_registered('jquery')) {
		// 	$out[] = $this->_script_tag(array(
		// 		'src'=>$this->path.'/js/jquery-1.8.2.min.js'
		// 	));
		// 	$this->register_component('jquery');
		// }

		if (!$this->component_registered('all-scripts')) {
			$out[] = $this->_script_tag(array(
				'src'=>$this->path.'/js/min/all-scripts-min.js'
			));
			$this->register_component('all-scripts');
		}
		
		// if (!$this->component_registered('flaunt')) {
		// 	$out[] = $this->_script_tag(array(
		// 		'src'=>$this->path.'/js/min/flaunt-min.js'
		// 	));
		// 	$this->register_component('flaunt');
		// }

		// if (!$this->component_registered('video-header')) {
		// 	$out[] = $this->_script_tag(array(
		// 		'src'=>$this->path.'/js/responsive.js'
		// 	));
		// 	$this->register_component('video-header');
		// }


		// if (!$this->component_registered('fonts')) {
		// 	$out[] = $this->_script_tag(array(
		// 		'src'=>$this->path.'/js/min/fonts-min.js'
		// 	));
		// 	$this->register_component('fonts');
		// }

		// if (!$this->component_registered('magnific-popup')) {
		// 	$out[] = $this->_script_tag(array(
		// 		'src'=>$this->path.'/js/magnific-popup.js'
		// 	));
		// 	$this->register_component('magnific-popup');
		// }

		// if (!$this->component_registered('jquery.fitvids')) {
		// 	$out[] = $this->_script_tag(array(
		// 		'src'=>$this->path.'/js/min/jquery.fitvids-min.js'
		// 	));
		// 	$this->register_component('jquery.fitvids');
		// }

		

		// if (!$this->component_registered('caption')) {
		// 	$out[] = $this->_script_tag(array(
		// 		'src'=>$this->path.'/js/jquery.caption.min.js'
		// 	));
		// 	$this->register_component('caption');
		// }

		// if (!$this->component_registered('caption-options')) {
		// 	$out[] = $this->_script_tag(array(
		// 		'src'=>$this->path.'/js/caption-options.js'
		// 	));
		// 	$this->register_component('caption-options');
		// }

		// if (!$this->component_registered('picturefill')) {
		// 	$out[] = $this->_script_tag(array(
		// 		'src'=>$this->path.'/js/picturefill.min.js'
		// 	));
		// 	$this->register_component('picturefill');
		// }


		

		

		return implode("\n\t", $out)."\n";
	}
}
?>