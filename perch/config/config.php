<?php

    include(__DIR__.'/config.local.php');  

    define('PERCH_LICENSE_KEY', 'R21412-YXJ244-YVJ115-XMR982-CYZ005');
    define('PERCH_EMAIL_FROM', 's.meehan@d3creative.uk');
    define('PERCH_EMAIL_FROM_NAME', 'Stephen Meehan');

    define('PERCH_LOGINPATH', '/perch');
    define('PERCH_PATH', str_replace(DIRECTORY_SEPARATOR.'config', '', __DIR__));
    define('PERCH_CORE', PERCH_PATH.DIRECTORY_SEPARATOR.'core');

    define('PERCH_RESFILEPATH', PERCH_PATH . DIRECTORY_SEPARATOR . 'resources');
    define('PERCH_RESPATH', PERCH_LOGINPATH . '/resources');
    
    define('PERCH_HTML5', true);
    define('PERCH_TZ', 'UTC');

    
    // define('PERCH_DEBUG', true);