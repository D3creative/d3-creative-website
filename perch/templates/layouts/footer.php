    <?php perch_get_javascript();?>



<?php if (perch_layout_var('call_to_action', true)) {
	perch_content('CTA Large');
}?>

<footer class="global-footer">



<section class="row footer-subnav">
	<div class="inner-container">

		
		<a href="/" title="Home page" class="brand-logo"> 
	 <img src="../perch/addons/feathers/d3feather/img/logo-x1.gif" srcset="../perch/addons/feathers/d3feather/img/logo-x2.gif 2x" alt="logo" >
	 </a> 
	
		<div class="sub-navigation">

			

			<?php perch_pages_navigation(array(
				'hide-extensions'      => true, 
			'navgroup'=>'footer-column-1', 
			'template' => 'item-footer.html'
			 ));?> 
	
			<?php perch_pages_navigation(array(
				'hide-extensions'      => true, 
			'navgroup'=>'footer-column-2', 
			'template' => 'item-footer.html'
			 ));?> 
	
			<?php perch_pages_navigation(array(
				'hide-extensions'      => true, 
			'navgroup'=>'footer-column-3', 
			'template' => 'item-footer-last.html'
			));?> 



		</div><!-- sub-navigation -->
		<p class="copyright">&copy; 2015. All Rights Reserved. <a title="Cookie Policy" href="/cookies/">Cookie Policy</a> <a title="Terms &amp; Conditions" href="/cookies/terms-and-conditions">Terms &amp; Conditions</a> <a title="Privacy Policy" href="/cookies/privacy-policy">Privacy Policy</a> </p>
	</div><!-- inner-container -->
</section>








</footer>






</body>
</html>









