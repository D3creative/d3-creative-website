<!doctype html>
<!--[if IE 6]>
<html class="ie6" lang="en" xmlns="http://www.w3.org/1999/xhtml">
<![endif]-->
<!--[if IE 7]>
<html class="ie7" lang="en" xmlns="http://www.w3.org/1999/xhtml">
<![endif]-->
<!--[if IE 8]>
<html class="ie8" lang="en" xmlns="http://www.w3.org/1999/xhtml>
<![endif]-->
<!--[if IE 9]>
<html class="ie9" lang="en" xmlns="http://www.w3.org/1999/xhtml>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
	<title><?php perch_pages_title(); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="format-detection" content="telephone=no" />
<meta name="format-detection" content="address=no" />
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="initial-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">

<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#3f6bb2">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">



	<?php perch_page_attributes(); ?>
	<?php perch_get_css(); ?>

<script>// Picture element HTML5 shiv
document.createElement( "picture" );
</script>
<script src="/perch/addons/feathers/d3feather/js/min/picturefill-min.js" async></script>

	<!-- Legacy -->
<!--[if lt IE 9]>
<script type="text/javascript" src="/perch/addons/feathers/d3feather/js/html5.js"></script>
<![endif]-->

   
<!--[if IE 6]>
<meta http-equiv="refresh" content="0; url=/ie6.html" />
<script type="text/javascript">
/* <![CDATA[ */
window.top.location = '/ie6.html';
/* ]]> */
</script>
<![endif]-->

 <!--[if IE 7]>
<meta http-equiv="refresh" content="0; url=/ie7.html" />
<script type="text/javascript">
/* <![CDATA[ */
window.top.location = '/ie7.html';
/* ]]> */
</script>
<![endif]-->
    
</head>

<?php 
$page = PerchSystem::get_page();
	
	if (perch_layout_has('mast-absolute')) {
		echo '<body class="'.PerchUtil::urlify(perch_page_attribute('pagePath', [], true)).' '.perch_layout_var('mast-absolute', true).'">';
	}
    if (perch_layout_has('mast-static')) {
		echo '<body class="'.PerchUtil::urlify(perch_page_attribute('pagePath', [], true)).''.perch_layout_var('mast-static', true).'">';
	}

	else{
		echo '<body>';
	}

	// if ($page == '/contact') { echo'<body class="contact">'; }
	// if ($page == '/happy-clients') { echo'<body class="happy-clients">'; }
	// if ($page == '/services') { echo'<body class="services">'; }
	// elseif (perch_layout_has('body-class')) {
	// 	echo '<body class="'.perch_layout_var('body-class', true).'">';
	// }

	// else{
	// 	echo '<body>';
	// }
?>