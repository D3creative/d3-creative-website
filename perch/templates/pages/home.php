<?php	
	// Include the header. You can find this in tempates/layouts/global
	perch_layout('head', [
		'body-class' => 'home',
		'mast-absolute' => 'mast-absolute',
	]);

    perch_layout('mast');

	// An editable content region
	perch_content('Hero');

   

    // An editable content region
    perch_content('Intro');

    perch_content('Latest project');

    perch_content('Call to action');
 	// Include the footer. You can find this in tempates/layouts/global
    perch_layout('footer');
