# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.42)
# Database: d3creative_
# Generation Time: 2015-07-08 16:32:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table perch2_backup_plans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_backup_plans`;

CREATE TABLE `perch2_backup_plans` (
  `planID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `planTitle` char(255) NOT NULL DEFAULT '',
  `planRole` enum('all','db') NOT NULL DEFAULT 'all',
  `planCreated` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `planCreatedBy` char(32) NOT NULL DEFAULT '',
  `planUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `planUpdatedBy` char(32) NOT NULL DEFAULT '',
  `planActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `planDynamicFields` text NOT NULL,
  `planFrequency` int(10) unsigned NOT NULL DEFAULT '24',
  `planBucket` char(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`planID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_backup_resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_backup_resources`;

CREATE TABLE `perch2_backup_resources` (
  `planID` int(10) unsigned NOT NULL,
  `resourceID` int(10) unsigned NOT NULL,
  `runID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`planID`,`resourceID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_backup_runs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_backup_runs`;

CREATE TABLE `perch2_backup_runs` (
  `runID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `planID` int(10) unsigned NOT NULL,
  `runDateTime` datetime NOT NULL,
  `runType` enum('db','resources') NOT NULL DEFAULT 'resources',
  `runResult` enum('OK','FAILED','IN PROGRESS') NOT NULL DEFAULT 'OK',
  `runMessage` char(255) NOT NULL DEFAULT '',
  `runDbFile` char(255) NOT NULL,
  PRIMARY KEY (`runID`),
  KEY `idx_plan` (`planID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_categories`;

CREATE TABLE `perch2_categories` (
  `catID` int(10) NOT NULL AUTO_INCREMENT,
  `setID` int(10) unsigned NOT NULL,
  `catParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `catTitle` char(64) NOT NULL DEFAULT '',
  `catSlug` char(64) NOT NULL DEFAULT '',
  `catPath` char(255) NOT NULL DEFAULT '',
  `catDisplayPath` char(255) NOT NULL DEFAULT '',
  `catOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `catTreePosition` char(255) NOT NULL DEFAULT '000',
  `catDynamicFields` text NOT NULL,
  PRIMARY KEY (`catID`),
  KEY `idx_set` (`setID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_category_counts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_category_counts`;

CREATE TABLE `perch2_category_counts` (
  `countID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catID` int(10) unsigned NOT NULL,
  `countType` char(64) NOT NULL DEFAULT '',
  `countValue` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`countID`),
  KEY `idx_cat` (`catID`),
  KEY `idx_cat_type` (`countType`,`catID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_category_sets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_category_sets`;

CREATE TABLE `perch2_category_sets` (
  `setID` int(10) NOT NULL AUTO_INCREMENT,
  `setTitle` char(64) NOT NULL DEFAULT '',
  `setSlug` char(64) NOT NULL DEFAULT '',
  `setTemplate` char(255) NOT NULL DEFAULT 'set.html',
  `setCatTemplate` char(255) NOT NULL DEFAULT 'category.html',
  `setDynamicFields` text,
  PRIMARY KEY (`setID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_collection_index
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_collection_index`;

CREATE TABLE `perch2_collection_index` (
  `indexID` int(10) NOT NULL AUTO_INCREMENT,
  `itemID` int(10) NOT NULL DEFAULT '0',
  `collectionID` int(10) NOT NULL DEFAULT '0',
  `itemRev` int(10) NOT NULL DEFAULT '0',
  `indexKey` char(64) NOT NULL DEFAULT '-',
  `indexValue` char(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`indexID`),
  KEY `idx_key` (`indexKey`),
  KEY `idx_val` (`indexValue`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_item` (`itemID`),
  KEY `idx_keyval` (`indexKey`,`indexValue`),
  KEY `idx_colrev` (`collectionID`,`itemRev`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_collection_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_collection_items`;

CREATE TABLE `perch2_collection_items` (
  `itemRowID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(10) unsigned NOT NULL,
  `itemRev` int(10) unsigned NOT NULL DEFAULT '0',
  `collectionID` int(10) unsigned NOT NULL,
  `itemJSON` mediumtext NOT NULL,
  `itemSearch` mediumtext NOT NULL,
  `itemUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `itemUpdatedBy` char(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemRowID`),
  KEY `idx_item` (`itemID`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_collection` (`collectionID`),
  KEY `idx_regrev` (`itemID`,`collectionID`,`itemRev`),
  FULLTEXT KEY `idx_search` (`itemSearch`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_collection_revisions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_collection_revisions`;

CREATE TABLE `perch2_collection_revisions` (
  `itemID` int(10) unsigned NOT NULL,
  `collectionID` int(10) unsigned NOT NULL,
  `itemOrder` int(10) unsigned DEFAULT '1000',
  `itemRev` int(10) unsigned NOT NULL,
  `itemLatestRev` int(10) unsigned NOT NULL,
  `itemCreated` datetime NOT NULL DEFAULT '2014-02-21 06:53:00',
  `itemCreatedBy` char(32) NOT NULL DEFAULT '',
  `itemSearchable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`itemID`),
  KEY `idx_order` (`itemOrder`),
  KEY `idx_searchable` (`itemSearchable`),
  KEY `idx_collection` (`collectionID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_collections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_collections`;

CREATE TABLE `perch2_collections` (
  `collectionID` int(10) NOT NULL AUTO_INCREMENT,
  `collectionKey` char(64) NOT NULL DEFAULT '',
  `collectionOrder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `collectionTemplate` char(255) NOT NULL DEFAULT '',
  `collectionOptions` text NOT NULL,
  `collectionSearchable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `collectionEditRoles` char(255) NOT NULL DEFAULT '*',
  `collectionUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `collectionInAppMenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`collectionID`),
  KEY `idx_key` (`collectionKey`),
  KEY `idx_appmenu` (`collectionInAppMenu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_content_index
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_content_index`;

CREATE TABLE `perch2_content_index` (
  `indexID` int(10) NOT NULL AUTO_INCREMENT,
  `itemID` int(10) NOT NULL DEFAULT '0',
  `regionID` int(10) NOT NULL DEFAULT '0',
  `pageID` int(10) NOT NULL DEFAULT '0',
  `itemRev` int(10) NOT NULL DEFAULT '0',
  `indexKey` char(64) NOT NULL DEFAULT '-',
  `indexValue` char(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`indexID`),
  KEY `idx_key` (`indexKey`),
  KEY `idx_val` (`indexValue`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_item` (`itemID`),
  KEY `idx_keyval` (`indexKey`,`indexValue`),
  KEY `idx_regrev` (`regionID`,`itemRev`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_content_index` WRITE;
/*!40000 ALTER TABLE `perch2_content_index` DISABLE KEYS */;

INSERT INTO `perch2_content_index` (`indexID`, `itemID`, `regionID`, `pageID`, `itemRev`, `indexKey`, `indexValue`)
VALUES
	(1,1,6,1,1,'text','Manchester-based website design and development services'),
	(2,1,6,1,1,'textarea','Mobile-friendly, content managed websites designed for business.'),
	(3,1,6,1,1,'align-text','left'),
	(4,1,6,1,1,'btn-txt-primary','Services'),
	(5,1,6,1,1,'btn-link-primary','/services'),
	(6,1,6,1,1,'btn-txt-secondary','Happy clients'),
	(7,1,6,1,1,'btn-link-secondary','/happy-clients'),
	(8,1,6,1,1,'image','/perch/resources/bubbles-girl-3.jpg'),
	(9,1,6,1,1,'imagecloseup','/perch/resources/bubbles-3.jpg'),
	(10,1,6,1,1,'alt','Girl blowing bubbles'),
	(11,1,6,1,1,'_id','1'),
	(12,1,6,1,1,'_order','1000'),
	(13,2,7,4,1,'text','Websites crafted for where your customers are - everywhere.'),
	(14,2,7,4,1,'textarea','A complete website solution'),
	(15,2,7,4,1,'align-text','left'),
	(16,2,7,4,1,'btn-txt-primary','Happy clients'),
	(17,2,7,4,1,'btn-link-primary','/happy-clients'),
	(18,2,7,4,1,'btn-txt-secondary','Contact us'),
	(19,2,7,4,1,'btn-link-secondary','/contact-us'),
	(20,2,7,4,1,'image','/perch/resources/services.png'),
	(21,2,7,4,1,'imagecloseup',''),
	(22,2,7,4,1,'alt','Creating a website'),
	(23,2,7,4,1,'_id','2'),
	(24,2,7,4,1,'_order','1000'),
	(25,3,11,5,1,'text','Making clients happy since 2005 we strive to be the best'),
	(26,3,11,5,1,'textarea','We design, develop and deliver'),
	(27,3,11,5,1,'align-text','left'),
	(28,3,11,5,1,'btn-txt-primary','Services'),
	(29,3,11,5,1,'btn-link-primary','/services'),
	(30,3,11,5,1,'btn-txt-secondary','Contact us'),
	(31,3,11,5,1,'btn-link-secondary','/contact'),
	(32,3,11,5,1,'image','/perch/resources/happy-clients.png'),
	(33,3,11,5,1,'imagecloseup',''),
	(34,3,11,5,1,'alt','Happy client'),
	(35,3,11,5,1,'_id','3'),
	(36,3,11,5,1,'_order','1000'),
	(37,4,13,6,1,'text','I welcome the opportunity to talk to you about your business'),
	(38,4,13,6,1,'textarea','Please complete all the fields below to get in touch'),
	(39,4,13,6,1,'align-text','left'),
	(40,4,13,6,1,'btn-txt-primary',''),
	(41,4,13,6,1,'btn-link-primary',''),
	(42,4,13,6,1,'btn-txt-secondary',''),
	(43,4,13,6,1,'btn-link-secondary',''),
	(44,4,13,6,1,'image','/perch/resources/contact.png'),
	(45,4,13,6,1,'imagecloseup',''),
	(46,4,13,6,1,'alt','Contact D3 Creative'),
	(47,4,13,6,1,'_id','4'),
	(48,4,13,6,1,'_order','1000'),
	(49,1,6,1,2,'text','Manchester-based website design and development services'),
	(50,1,6,1,2,'textarea','Mobile-friendly, content managed websites designed for business.'),
	(51,1,6,1,2,'align-text','left'),
	(52,1,6,1,2,'btn-txt-primary','Services'),
	(53,1,6,1,2,'btn-link-primary','/services'),
	(54,1,6,1,2,'btn-txt-secondary','Happy clients'),
	(55,1,6,1,2,'btn-link-secondary','/happy-clients'),
	(56,1,6,1,2,'image','/perch/resources/bubbles-girl-3.jpg'),
	(57,1,6,1,2,'imagecloseup','/perch/resources/bubbles-3.jpg'),
	(58,1,6,1,2,'alt','Girl blowing bubbles'),
	(59,1,6,1,2,'_id','1'),
	(60,1,6,1,2,'_order','1000'),
	(61,2,7,4,2,'text','Websites crafted for where your customers are - everywhere.'),
	(62,2,7,4,2,'textarea','A complete website solution'),
	(63,2,7,4,2,'align-text','left'),
	(64,2,7,4,2,'btn-txt-primary','Happy clients'),
	(65,2,7,4,2,'btn-link-primary','/happy-clients'),
	(66,2,7,4,2,'btn-txt-secondary','Contact us'),
	(67,2,7,4,2,'btn-link-secondary','/contact-us'),
	(68,2,7,4,2,'image','/perch/resources/services.png'),
	(69,2,7,4,2,'imagecloseup',''),
	(70,2,7,4,2,'alt','Creating a website'),
	(71,2,7,4,2,'_id','2'),
	(72,2,7,4,2,'_order','1000'),
	(73,3,11,5,2,'text','Making clients happy since 2005 we strive to be the best'),
	(74,3,11,5,2,'textarea','We design, develop and deliver'),
	(75,3,11,5,2,'align-text','left'),
	(76,3,11,5,2,'btn-txt-primary','Services'),
	(77,3,11,5,2,'btn-link-primary','/services'),
	(78,3,11,5,2,'btn-txt-secondary','Contact us'),
	(79,3,11,5,2,'btn-link-secondary','/contact'),
	(80,3,11,5,2,'image','/perch/resources/happy-clients.png'),
	(81,3,11,5,2,'imagecloseup',''),
	(82,3,11,5,2,'alt','Happy client'),
	(83,3,11,5,2,'_id','3'),
	(84,3,11,5,2,'_order','1000'),
	(85,4,13,6,2,'text','I welcome the opportunity to talk to you about your business'),
	(86,4,13,6,2,'textarea','Please complete all the fields below to get in touch'),
	(87,4,13,6,2,'align-text','left'),
	(88,4,13,6,2,'btn-txt-primary',''),
	(89,4,13,6,2,'btn-link-primary',''),
	(90,4,13,6,2,'btn-txt-secondary',''),
	(91,4,13,6,2,'btn-link-secondary',''),
	(92,4,13,6,2,'image','/perch/resources/contact.png'),
	(93,4,13,6,2,'imagecloseup',''),
	(94,4,13,6,2,'alt','Contact D3 Creative'),
	(95,4,13,6,2,'_id','4'),
	(96,4,13,6,2,'_order','1000'),
	(97,1,6,1,3,'text','Manchester-based website design and development services'),
	(98,1,6,1,3,'textarea','Mobile-friendly, content managed websites designed for business.'),
	(99,1,6,1,3,'align-text','left'),
	(100,1,6,1,3,'btn-txt-primary','Services'),
	(101,1,6,1,3,'btn-link-primary','/services'),
	(102,1,6,1,3,'btn-txt-secondary','Happy clients'),
	(103,1,6,1,3,'btn-link-secondary','/happy-clients'),
	(104,1,6,1,3,'image','/perch/resources/bubbles-girl-3.jpg'),
	(105,1,6,1,3,'imagecloseup','/perch/resources/bubbles-3.jpg'),
	(106,1,6,1,3,'alt','Girl blowing bubbles'),
	(107,1,6,1,3,'_id','1'),
	(108,1,6,1,3,'_order','1000'),
	(109,1,6,1,4,'text','Manchester-based website design and development services'),
	(110,1,6,1,4,'textarea','Mobile-friendly, content managed websites designed for business.'),
	(111,1,6,1,4,'align-text','left'),
	(112,1,6,1,4,'btn-txt-primary','Services'),
	(113,1,6,1,4,'btn-link-primary','/services'),
	(114,1,6,1,4,'btn-txt-secondary','Happy clients'),
	(115,1,6,1,4,'btn-link-secondary','/happy-clients'),
	(116,1,6,1,4,'image','/perch/resources/bubbles-girl-3.jpg'),
	(117,1,6,1,4,'imagecloseup','/perch/resources/bubbles-3.jpg'),
	(118,1,6,1,4,'alt','Girl blowing bubbles'),
	(119,1,6,1,4,'_id','1'),
	(120,1,6,1,4,'_order','1000'),
	(121,1,6,1,5,'text','Manchester-based website design and development services'),
	(122,1,6,1,5,'textarea','Mobile-friendly, content managed websites designed for business.'),
	(123,1,6,1,5,'align-text','left'),
	(124,1,6,1,5,'btn-txt-primary','Services'),
	(125,1,6,1,5,'btn-link-primary','/services'),
	(126,1,6,1,5,'btn-txt-secondary','Happy clients'),
	(127,1,6,1,5,'btn-link-secondary','/happy-clients'),
	(128,1,6,1,5,'image','/perch/resources/bubbles-girl-3.jpg'),
	(129,1,6,1,5,'imagecloseup','/perch/resources/bubbles-3.jpg'),
	(130,1,6,1,5,'alt','Girl blowing bubbles'),
	(131,1,6,1,5,'_id','1'),
	(132,1,6,1,5,'_order','1000'),
	(133,2,7,4,3,'text','Websites crafted for where your customers are - everywhere.'),
	(134,2,7,4,3,'textarea','A complete website solution'),
	(135,2,7,4,3,'align-text','left'),
	(136,2,7,4,3,'btn-txt-primary','Happy clients'),
	(137,2,7,4,3,'btn-link-primary','/happy-clients'),
	(138,2,7,4,3,'btn-txt-secondary','Contact us'),
	(139,2,7,4,3,'btn-link-secondary','/contact'),
	(140,2,7,4,3,'image','/perch/resources/services.png'),
	(141,2,7,4,3,'imagecloseup',''),
	(142,2,7,4,3,'alt','Creating a website'),
	(143,2,7,4,3,'_id','2'),
	(144,2,7,4,3,'_order','1000'),
	(145,5,21,6,1,'company-name','D3 Creative'),
	(146,5,21,6,1,'address-line-1','Studio 40,'),
	(147,5,21,6,1,'address-line-2','792 Wilmslow Road,'),
	(148,5,21,6,1,'address-line-3','Didsbury,'),
	(149,5,21,6,1,'address-line-4','Manchester.'),
	(150,5,21,6,1,'address-line-5',''),
	(151,5,21,6,1,'postcode','M20 6UG'),
	(152,5,21,6,1,'phone-number','0161 241 8050'),
	(153,5,21,6,1,'fax-number',''),
	(154,5,21,6,1,'mobile-number','07815 105465'),
	(155,5,21,6,1,'email-address-1','s.meehan@d3creative.uk'),
	(156,5,21,6,1,'media-company-name',''),
	(157,5,21,6,1,'media-name',''),
	(158,5,21,6,1,'media-phone-number',''),
	(159,5,21,6,1,'email-address',''),
	(160,5,21,6,1,'_id','5'),
	(161,5,21,6,1,'_order','1000'),
	(162,5,21,6,2,'company-name','D3 Creative'),
	(163,5,21,6,2,'address-line-1','Studio 40,'),
	(164,5,21,6,2,'address-line-2','792 Wilmslow Road,'),
	(165,5,21,6,2,'address-line-3','Didsbury,'),
	(166,5,21,6,2,'address-line-4','Manchester.'),
	(167,5,21,6,2,'address-line-5',''),
	(168,5,21,6,2,'postcode','M20 6UG'),
	(169,5,21,6,2,'phone-number','0161 241 8050'),
	(170,5,21,6,2,'fax-number',''),
	(171,5,21,6,2,'mobile-number','07815 105465'),
	(172,5,21,6,2,'email-address-1','s.meehan@d3creative.uk'),
	(173,5,21,6,2,'media-company-name',''),
	(174,5,21,6,2,'media-name',''),
	(175,5,21,6,2,'media-phone-number',''),
	(176,5,21,6,2,'email-address',''),
	(177,5,21,6,2,'_id','5'),
	(178,5,21,6,2,'_order','1000'),
	(236,6,14,6,17,'smallprint','Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.'),
	(235,6,14,6,17,'form-intro','A member of our team will endeavour to contact you as soon as possible.'),
	(241,6,14,6,18,'smallprint','Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.'),
	(240,6,14,6,18,'form-intro','A member of our team will endeavour to contact you as soon as possible.'),
	(264,6,14,6,19,'address-line-1',''),
	(263,6,14,6,19,'company-name',''),
	(284,6,14,6,20,'_order','1000'),
	(283,6,14,6,20,'_id','6'),
	(340,6,14,6,21,'_order','1000'),
	(339,6,14,6,21,'_id','6'),
	(345,6,14,6,22,'_order','1000'),
	(344,6,14,6,22,'_id','6'),
	(367,6,14,6,23,'_order','1000'),
	(366,6,14,6,23,'_id','6'),
	(231,6,14,6,16,'smallprint','Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.'),
	(230,6,14,6,16,'form-intro','A member of our team will endeavour to contact you as soon as possible.'),
	(262,6,14,6,19,'success','Thank you for taking the time to complete this form.'),
	(261,6,14,6,19,'smallprint','Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.'),
	(260,6,14,6,19,'form-intro','A member of our team will endeavour to contact you as soon as possible.'),
	(282,6,14,6,20,'success','Thank you for taking the time to complete this form.'),
	(281,6,14,6,20,'smallprint','Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.'),
	(280,6,14,6,20,'form-intro','A member of our team will endeavour to contact you as soon as possible.'),
	(338,6,14,6,21,'success','Thank you for taking the time to complete this form.'),
	(337,6,14,6,21,'smallprint','Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.'),
	(336,6,14,6,21,'form-intro','A member of our team will endeavour to contact you as soon as possible.'),
	(343,6,14,6,22,'success','Thank you for taking the time to complete this form.'),
	(342,6,14,6,22,'smallprint','Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.'),
	(341,6,14,6,22,'form-intro','Really great communication is the key to a successful project, let’s start as we mean to go on:'),
	(365,6,14,6,23,'success','Thank you for taking the time to complete this form.'),
	(364,6,14,6,23,'smallprint','Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.'),
	(363,6,14,6,23,'form-intro','Really great communication is the key to a successful project, let’s start as we mean to go on:'),
	(232,6,14,6,16,'success','Thank you for taking the time to complete this form.'),
	(233,6,14,6,16,'_id','6'),
	(234,6,14,6,16,'_order','1000'),
	(237,6,14,6,17,'success','Thank you for taking the time to complete this form.'),
	(238,6,14,6,17,'_id','6'),
	(239,6,14,6,17,'_order','1000'),
	(242,6,14,6,18,'success','Thank you for taking the time to complete this form.'),
	(243,6,14,6,18,'company-name',''),
	(244,6,14,6,18,'address-line-1',''),
	(245,6,14,6,18,'address-line-2',''),
	(246,6,14,6,18,'address-line-3',''),
	(247,6,14,6,18,'address-line-4',''),
	(248,6,14,6,18,'address-line-5',''),
	(249,6,14,6,18,'postcode',''),
	(250,6,14,6,18,'phone-number',''),
	(251,6,14,6,18,'fax-number',''),
	(252,6,14,6,18,'mobile-number',''),
	(253,6,14,6,18,'email-address-1',''),
	(254,6,14,6,18,'media-company-name',''),
	(255,6,14,6,18,'media-name',''),
	(256,6,14,6,18,'media-phone-number',''),
	(257,6,14,6,18,'email-address',''),
	(258,6,14,6,18,'_id','6'),
	(259,6,14,6,18,'_order','1000'),
	(265,6,14,6,19,'address-line-2',''),
	(266,6,14,6,19,'address-line-3',''),
	(267,6,14,6,19,'address-line-4',''),
	(268,6,14,6,19,'address-line-5',''),
	(269,6,14,6,19,'postcode',''),
	(270,6,14,6,19,'phone-number',''),
	(271,6,14,6,19,'fax-number',''),
	(272,6,14,6,19,'mobile-number',''),
	(273,6,14,6,19,'email-address-1',''),
	(274,6,14,6,19,'media-company-name',''),
	(275,6,14,6,19,'media-name',''),
	(276,6,14,6,19,'media-phone-number',''),
	(277,6,14,6,19,'email-address',''),
	(278,6,14,6,19,'_id','6'),
	(279,6,14,6,19,'_order','1000'),
	(285,5,21,6,3,'company-name','D3 Creative WOO'),
	(286,5,21,6,3,'address-line-1','Studio 40,'),
	(287,5,21,6,3,'address-line-2','792 Wilmslow Road,'),
	(288,5,21,6,3,'address-line-3','Didsbury,'),
	(289,5,21,6,3,'address-line-4','Manchester.'),
	(290,5,21,6,3,'address-line-5',''),
	(291,5,21,6,3,'postcode','M20 6UG'),
	(292,5,21,6,3,'phone-number','0161 241 8050'),
	(293,5,21,6,3,'fax-number',''),
	(294,5,21,6,3,'mobile-number','07815 105465'),
	(295,5,21,6,3,'email-address-1','s.meehan@d3creative.uk'),
	(296,5,21,6,3,'media-company-name',''),
	(297,5,21,6,3,'media-name',''),
	(298,5,21,6,3,'media-phone-number',''),
	(299,5,21,6,3,'email-address',''),
	(300,5,21,6,3,'_id','5'),
	(301,5,21,6,3,'_order','1000'),
	(302,5,21,6,4,'company-name','D3 Creative'),
	(303,5,21,6,4,'address-line-1','Studio 40,'),
	(304,5,21,6,4,'address-line-2','792 Wilmslow Road,'),
	(305,5,21,6,4,'address-line-3','Didsbury,'),
	(306,5,21,6,4,'address-line-4','Manchester.'),
	(307,5,21,6,4,'address-line-5',''),
	(308,5,21,6,4,'postcode','M20 6UG'),
	(309,5,21,6,4,'phone-number','0161 241 8050'),
	(310,5,21,6,4,'fax-number',''),
	(311,5,21,6,4,'mobile-number','07815 105465'),
	(312,5,21,6,4,'email-address-1','s.meehan@d3creative.uk'),
	(313,5,21,6,4,'media-company-name',''),
	(314,5,21,6,4,'media-name',''),
	(315,5,21,6,4,'media-phone-number',''),
	(316,5,21,6,4,'email-address',''),
	(317,5,21,6,4,'_id','5'),
	(318,5,21,6,4,'_order','1000'),
	(319,5,21,6,5,'company-name','D3 Creative'),
	(320,5,21,6,5,'address-line-1','Studio 40,'),
	(321,5,21,6,5,'address-line-2','792 Wilmslow Road,'),
	(322,5,21,6,5,'address-line-3','Didsbury,'),
	(323,5,21,6,5,'address-line-4','Manchester.'),
	(324,5,21,6,5,'address-line-5',''),
	(325,5,21,6,5,'postcode','M20 6UG'),
	(326,5,21,6,5,'phone-number','0161 241 8050'),
	(327,5,21,6,5,'fax-number',''),
	(328,5,21,6,5,'mobile-number','07815 105465'),
	(329,5,21,6,5,'email-address-1','s.meehan@d3creative.uk'),
	(330,5,21,6,5,'media-company-name',''),
	(331,5,21,6,5,'media-name',''),
	(332,5,21,6,5,'media-phone-number',''),
	(333,5,21,6,5,'email-address',''),
	(334,5,21,6,5,'_id','5'),
	(335,5,21,6,5,'_order','1000'),
	(346,5,21,6,6,'company-name','D3 Creative'),
	(347,5,21,6,6,'address-line-1','Studio 40,'),
	(348,5,21,6,6,'address-line-2','792 Wilmslow Road,'),
	(349,5,21,6,6,'address-line-3','Didsbury,'),
	(350,5,21,6,6,'address-line-4','Manchester.'),
	(351,5,21,6,6,'address-line-5',''),
	(352,5,21,6,6,'postcode','M20 6UG'),
	(353,5,21,6,6,'phone-number','0161 241 8050'),
	(354,5,21,6,6,'fax-number',''),
	(355,5,21,6,6,'mobile-number','07815 105465'),
	(356,5,21,6,6,'email-address-1','s.meehan@d3creative.uk'),
	(357,5,21,6,6,'media-company-name',''),
	(358,5,21,6,6,'media-name',''),
	(359,5,21,6,6,'media-phone-number',''),
	(360,5,21,6,6,'email-address',''),
	(361,5,21,6,6,'_id','5'),
	(362,5,21,6,6,'_order','1000'),
	(385,7,2,1,9,'_order','1000'),
	(384,7,2,1,9,'_id','7'),
	(387,7,2,1,10,'_order','1000'),
	(386,7,2,1,10,'_id','7'),
	(372,7,2,1,3,'_id','7'),
	(373,7,2,1,3,'_order','1000'),
	(374,7,2,1,4,'_id','7'),
	(375,7,2,1,4,'_order','1000'),
	(376,7,2,1,5,'_id','7'),
	(377,7,2,1,5,'_order','1000'),
	(378,7,2,1,6,'_id','7'),
	(379,7,2,1,6,'_order','1000'),
	(380,7,2,1,7,'_id','7'),
	(381,7,2,1,7,'_order','1000'),
	(382,7,2,1,8,'_id','7'),
	(383,7,2,1,8,'_order','1000');

/*!40000 ALTER TABLE `perch2_content_index` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_content_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_content_items`;

CREATE TABLE `perch2_content_items` (
  `itemRowID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(10) unsigned NOT NULL DEFAULT '0',
  `regionID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageID` int(10) unsigned NOT NULL DEFAULT '0',
  `itemRev` int(10) unsigned NOT NULL DEFAULT '0',
  `itemOrder` int(10) unsigned NOT NULL DEFAULT '1000',
  `itemJSON` mediumtext NOT NULL,
  `itemSearch` mediumtext NOT NULL,
  `itemUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `itemUpdatedBy` char(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemRowID`),
  KEY `idx_item` (`itemID`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_region` (`regionID`),
  KEY `idx_regrev` (`itemID`,`regionID`,`itemRev`),
  KEY `idx_order` (`itemOrder`),
  FULLTEXT KEY `idx_search` (`itemSearch`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_content_items` WRITE;
/*!40000 ALTER TABLE `perch2_content_items` DISABLE KEYS */;

INSERT INTO `perch2_content_items` (`itemRowID`, `itemID`, `regionID`, `pageID`, `itemRev`, `itemOrder`, `itemJSON`, `itemSearch`, `itemUpdated`, `itemUpdatedBy`)
VALUES
	(1,1,6,1,0,1000,'','','2015-07-07 14:51:23',''),
	(2,1,6,1,1,1000,'{\"_id\":\"1\",\"text\":\"Manchester-based website design and development services\",\"_title\":\"Manchester-based website design and development services \\/services \\/happy-clients Girl blowing bubbles\",\"textarea\":{\"raw\":\"Mobile-friendly, content managed websites designed for business.\",\"processed\":\"<p>Mobile-friendly, content managed websites designed for business.<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Services\",\"btn-link-primary\":\"\\/services\",\"btn-txt-secondary\":\"Happy clients\",\"btn-link-secondary\":\"\\/happy-clients\",\"image\":{\"assetID\":\"1\",\"title\":\"Bubbles girl 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-girl-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-girl-3.jpg\",\"size\":129821,\"w\":1400,\"h\":550,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"58\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-girl-3-thumb@2x.jpg\",\"size\":10515,\"mime\":\"\",\"assetID\":\"2\"},\"w200hc0\":{\"w\":200,\"h\":78,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w200.jpg\",\"size\":5648,\"mime\":\"image\\/jpeg\"},\"w400hc0\":{\"w\":400,\"h\":157,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w400.jpg\",\"size\":16023,\"mime\":\"image\\/jpeg\"},\"w800hc0\":{\"w\":800,\"h\":314,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w800.jpg\",\"size\":48158,\"mime\":\"image\\/jpeg\"},\"w1200hc0\":{\"w\":1200,\"h\":471,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1200.jpg\",\"size\":88792,\"mime\":\"image\\/jpeg\"},\"w1600hc0\":{\"w\":1400,\"h\":550,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1600.jpg\",\"size\":129821,\"mime\":\"\"},\"w2000hc0\":{\"w\":1400,\"h\":550,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w2000.jpg\",\"size\":129821,\"mime\":\"\"}}},\"imagecloseup\":{\"assetID\":\"3\",\"title\":\"Bubbles 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-3.jpg\",\"size\":42220,\"w\":800,\"h\":470,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"88\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-3-thumb@2x.jpg\",\"size\":9144,\"mime\":\"\",\"assetID\":\"4\"},\"w200hc0\":{\"w\":200,\"h\":117,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w200.jpg\",\"size\":5375,\"mime\":\"image\\/jpeg\"},\"w400hc0\":{\"w\":400,\"h\":235,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w400.jpg\",\"size\":14133,\"mime\":\"image\\/jpeg\"},\"w800hc0\":{\"w\":800,\"h\":470,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w800.jpg\",\"size\":42220,\"mime\":\"\"},\"w1200hc0\":{\"w\":800,\"h\":470,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1200.jpg\",\"size\":42220,\"mime\":\"\"},\"w1600hc0\":{\"w\":800,\"h\":470,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1600.jpg\",\"size\":42220,\"mime\":\"\"},\"w2000hc0\":{\"w\":800,\"h\":470,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w2000.jpg\",\"size\":42220,\"mime\":\"\"}}},\"alt\":\"Girl blowing bubbles\"}',' Manchester-based website design and development services Mobile-friendly, content managed websites designed for business. left Services /services Happy clients /happy-clients   Girl blowing bubbles ','2015-07-07 14:53:18','1'),
	(3,2,7,4,0,1000,'','','2015-07-07 16:02:43',''),
	(4,2,7,4,1,1000,'{\"_id\":\"2\",\"text\":\"Websites crafted for where your customers are - everywhere.\",\"_title\":\"Websites crafted for where your customers are - everywhere. \\/happy-clients \\/contact-us Creating a website\",\"textarea\":{\"raw\":\"A complete website solution\",\"processed\":\"<p>A complete website solution<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Happy clients\",\"btn-link-primary\":\"\\/happy-clients\",\"btn-txt-secondary\":\"Contact us\",\"btn-link-secondary\":\"\\/contact-us\",\"image\":{\"assetID\":\"17\",\"title\":\"Services\",\"_default\":\"\\/perch\\/resources\\/services.png\",\"bucket\":\"default\",\"path\":\"services.png\",\"size\":1687877,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"services-thumb@2x.png\",\"size\":58154,\"mime\":\"\",\"assetID\":\"18\"},\"w200hc0\":{\"w\":200,\"h\":95,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w200.png\",\"size\":27994,\"mime\":\"image\\/png\"},\"w400hc0\":{\"w\":400,\"h\":191,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w400.png\",\"size\":97775,\"mime\":\"image\\/png\"},\"w800hc0\":{\"w\":800,\"h\":383,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w800.png\",\"size\":349747,\"mime\":\"image\\/png\"},\"w1200hc0\":{\"w\":1200,\"h\":575,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w1200.png\",\"size\":738819,\"mime\":\"image\\/png\"},\"w1600hc0\":{\"w\":1600,\"h\":767,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w1600.png\",\"size\":1239582,\"mime\":\"image\\/png\"},\"w2000hc0\":{\"w\":2000,\"h\":959,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w2000.png\",\"size\":1821700,\"mime\":\"image\\/png\"}}},\"imagecloseup\":null,\"alt\":\"Creating a website\"}',' Websites crafted for where your customers are - everywhere. A complete website solution left Happy clients /happy-clients Contact us /contact-us   Creating a website ','2015-07-07 16:04:31','1'),
	(5,3,11,5,0,1000,'','','2015-07-07 16:08:21',''),
	(6,3,11,5,1,1000,'{\"_id\":\"3\",\"text\":\"Making clients happy since 2005 we strive to be the best\",\"_title\":\"Making clients happy since 2005 we strive to be the best \\/services \\/contact Happy client\",\"textarea\":{\"raw\":\"We design, develop and deliver\",\"processed\":\"<p>We design, develop and deliver<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Services\",\"btn-link-primary\":\"\\/services\",\"btn-txt-secondary\":\"Contact us\",\"btn-link-secondary\":\"\\/contact\",\"image\":{\"assetID\":\"25\",\"title\":\"Happy clients\",\"_default\":\"\\/perch\\/resources\\/happy-clients.png\",\"bucket\":\"default\",\"path\":\"happy-clients.png\",\"size\":1855777,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"happy-clients-thumb@2x.png\",\"size\":66056,\"mime\":\"\",\"assetID\":\"26\"},\"w200hc0\":{\"w\":200,\"h\":95,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w200.png\",\"size\":33674,\"mime\":\"image\\/png\"},\"w400hc0\":{\"w\":400,\"h\":191,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w400.png\",\"size\":107139,\"mime\":\"image\\/png\"},\"w800hc0\":{\"w\":800,\"h\":383,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w800.png\",\"size\":359700,\"mime\":\"image\\/png\"},\"w1200hc0\":{\"w\":1200,\"h\":575,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w1200.png\",\"size\":760323,\"mime\":\"image\\/png\"},\"w1600hc0\":{\"w\":1600,\"h\":767,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w1600.png\",\"size\":1292617,\"mime\":\"image\\/png\"},\"w2000hc0\":{\"w\":2000,\"h\":959,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w2000.png\",\"size\":1914935,\"mime\":\"image\\/png\"}}},\"imagecloseup\":null,\"alt\":\"Happy client\"}',' Making clients happy since 2005 we strive to be the best We design, develop and deliver left Services /services Contact us /contact   Happy client ','2015-07-07 16:09:39','1'),
	(7,4,13,6,0,1000,'','','2015-07-07 16:15:04',''),
	(8,4,13,6,1,1000,'{\"_id\":\"4\",\"text\":\"I welcome the opportunity to talk to you about your business\",\"_title\":\"I welcome the opportunity to talk to you about your business Contact D3 Creative\",\"textarea\":{\"raw\":\"Please complete all the fields below to get in touch\",\"processed\":\"<p>Please complete all the fields below to get in touch<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":null,\"btn-link-primary\":null,\"btn-txt-secondary\":null,\"btn-link-secondary\":null,\"image\":{\"assetID\":\"33\",\"title\":\"Contact\",\"_default\":\"\\/perch\\/resources\\/contact.png\",\"bucket\":\"default\",\"path\":\"contact.png\",\"size\":1969945,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"contact-thumb@2x.png\",\"size\":71297,\"mime\":\"\",\"assetID\":\"34\"},\"w200hc0\":{\"w\":200,\"h\":95,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w200.png\",\"size\":34959,\"mime\":\"image\\/png\"},\"w400hc0\":{\"w\":400,\"h\":191,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w400.png\",\"size\":117732,\"mime\":\"image\\/png\"},\"w800hc0\":{\"w\":800,\"h\":383,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w800.png\",\"size\":390771,\"mime\":\"image\\/png\"},\"w1200hc0\":{\"w\":1200,\"h\":575,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w1200.png\",\"size\":800902,\"mime\":\"image\\/png\"},\"w1600hc0\":{\"w\":1600,\"h\":767,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w1600.png\",\"size\":1338008,\"mime\":\"image\\/png\"},\"w2000hc0\":{\"w\":2000,\"h\":959,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w2000.png\",\"size\":1978480,\"mime\":\"image\\/png\"}}},\"imagecloseup\":null,\"alt\":\"Contact D3 Creative\"}',' I welcome the opportunity to talk to you about your business Please complete all the fields below to get in touch left       Contact D3 Creative ','2015-07-07 16:15:59','1'),
	(9,1,6,1,2,1000,'{\"_id\":\"1\",\"text\":\"Manchester-based website design and development services\",\"_title\":\"Manchester-based website design and development services \\/services \\/happy-clients Girl blowing bubbles\",\"textarea\":{\"raw\":\"Mobile-friendly, content managed websites designed for business.\",\"processed\":\"<p>Mobile-friendly, content managed websites designed for business.<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Services\",\"btn-link-primary\":\"\\/services\",\"btn-txt-secondary\":\"Happy clients\",\"btn-link-secondary\":\"\\/happy-clients\",\"image\":{\"assetID\":\"1\",\"title\":\"Bubbles girl 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-girl-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-girl-3.jpg\",\"size\":129821,\"w\":1400,\"h\":550,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"58\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-girl-3-thumb@2x.jpg\",\"size\":10515,\"mime\":\"\",\"assetID\":\"2\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"78\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w200.jpg\",\"size\":5648,\"mime\":\"\",\"assetID\":\"5\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"157\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w400.jpg\",\"size\":16023,\"mime\":\"\",\"assetID\":\"6\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"314\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w800.jpg\",\"size\":48158,\"mime\":\"\",\"assetID\":\"7\"},\"w1200hc0\":{\"w\":\"1200\",\"h\":\"471\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1200.jpg\",\"size\":88792,\"mime\":\"\",\"assetID\":\"8\"},\"w1600hc0\":{\"w\":\"1400\",\"h\":\"550\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1600.jpg\",\"size\":129821,\"mime\":\"\",\"assetID\":\"9\"},\"w2000hc0\":{\"w\":\"1400\",\"h\":\"550\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w2000.jpg\",\"size\":129821,\"mime\":\"\",\"assetID\":\"10\"}}},\"imagecloseup\":{\"assetID\":\"3\",\"title\":\"Bubbles 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-3.jpg\",\"size\":42220,\"w\":800,\"h\":470,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"88\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-3-thumb@2x.jpg\",\"size\":9144,\"mime\":\"\",\"assetID\":\"4\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"117\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w200.jpg\",\"size\":5375,\"mime\":\"\",\"assetID\":\"11\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"235\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w400.jpg\",\"size\":14133,\"mime\":\"\",\"assetID\":\"12\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w800.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"13\"},\"w1200hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1200.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"14\"},\"w1600hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1600.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"15\"},\"w2000hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w2000.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"16\"}}},\"alt\":\"Girl blowing bubbles\"}',' Manchester-based website design and development services Mobile-friendly, content managed websites designed for business. left Services /services Happy clients /happy-clients   Girl blowing bubbles ','2015-07-08 10:22:58','1'),
	(10,2,7,4,2,1000,'{\"_id\":\"2\",\"text\":\"Websites crafted for where your customers are - everywhere.\",\"_title\":\"Websites crafted for where your customers are - everywhere. \\/happy-clients \\/contact-us Creating a website\",\"textarea\":{\"raw\":\"A complete website solution\",\"processed\":\"<p>A complete website solution<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Happy clients\",\"btn-link-primary\":\"\\/happy-clients\",\"btn-txt-secondary\":\"Contact us\",\"btn-link-secondary\":\"\\/contact-us\",\"image\":{\"assetID\":\"17\",\"title\":\"Services\",\"_default\":\"\\/perch\\/resources\\/services.png\",\"bucket\":\"default\",\"path\":\"services.png\",\"size\":1687877,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"services-thumb@2x.png\",\"size\":58154,\"mime\":\"\",\"assetID\":\"18\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"95\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w200.png\",\"size\":27994,\"mime\":\"\",\"assetID\":\"19\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"191\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w400.png\",\"size\":97775,\"mime\":\"\",\"assetID\":\"20\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"383\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w800.png\",\"size\":349747,\"mime\":\"\",\"assetID\":\"21\"},\"w1200hc0\":{\"w\":\"1200\",\"h\":\"575\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w1200.png\",\"size\":738819,\"mime\":\"\",\"assetID\":\"22\"},\"w1600hc0\":{\"w\":\"1600\",\"h\":\"767\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w1600.png\",\"size\":1239582,\"mime\":\"\",\"assetID\":\"23\"},\"w2000hc0\":{\"w\":\"2000\",\"h\":\"959\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w2000.png\",\"size\":1821700,\"mime\":\"\",\"assetID\":\"24\"}}},\"imagecloseup\":null,\"alt\":\"Creating a website\"}',' Websites crafted for where your customers are - everywhere. A complete website solution left Happy clients /happy-clients Contact us /contact-us   Creating a website ','2015-07-08 10:23:08','1'),
	(11,3,11,5,2,1000,'{\"_id\":\"3\",\"text\":\"Making clients happy since 2005 we strive to be the best\",\"_title\":\"Making clients happy since 2005 we strive to be the best \\/services \\/contact Happy client\",\"textarea\":{\"raw\":\"We design, develop and deliver\",\"processed\":\"<p>We design, develop and deliver<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Services\",\"btn-link-primary\":\"\\/services\",\"btn-txt-secondary\":\"Contact us\",\"btn-link-secondary\":\"\\/contact\",\"image\":{\"assetID\":\"25\",\"title\":\"Happy clients\",\"_default\":\"\\/perch\\/resources\\/happy-clients.png\",\"bucket\":\"default\",\"path\":\"happy-clients.png\",\"size\":1855777,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"happy-clients-thumb@2x.png\",\"size\":66056,\"mime\":\"\",\"assetID\":\"26\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"95\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w200.png\",\"size\":33674,\"mime\":\"\",\"assetID\":\"27\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"191\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w400.png\",\"size\":107139,\"mime\":\"\",\"assetID\":\"28\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"383\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w800.png\",\"size\":359700,\"mime\":\"\",\"assetID\":\"29\"},\"w1200hc0\":{\"w\":\"1200\",\"h\":\"575\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w1200.png\",\"size\":760323,\"mime\":\"\",\"assetID\":\"30\"},\"w1600hc0\":{\"w\":\"1600\",\"h\":\"767\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w1600.png\",\"size\":1292617,\"mime\":\"\",\"assetID\":\"31\"},\"w2000hc0\":{\"w\":\"2000\",\"h\":\"959\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w2000.png\",\"size\":1914935,\"mime\":\"\",\"assetID\":\"32\"}}},\"imagecloseup\":null,\"alt\":\"Happy client\"}',' Making clients happy since 2005 we strive to be the best We design, develop and deliver left Services /services Contact us /contact   Happy client ','2015-07-08 10:24:19','1'),
	(12,4,13,6,2,1000,'{\"_id\":\"4\",\"text\":\"I welcome the opportunity to talk to you about your business\",\"_title\":\"I welcome the opportunity to talk to you about your business Contact D3 Creative\",\"textarea\":{\"raw\":\"Please complete all the fields below to get in touch\",\"processed\":\"<p>Please complete all the fields below to get in touch<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":null,\"btn-link-primary\":null,\"btn-txt-secondary\":null,\"btn-link-secondary\":null,\"image\":{\"assetID\":\"33\",\"title\":\"Contact\",\"_default\":\"\\/perch\\/resources\\/contact.png\",\"bucket\":\"default\",\"path\":\"contact.png\",\"size\":1969945,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"contact-thumb@2x.png\",\"size\":71297,\"mime\":\"\",\"assetID\":\"34\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"95\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w200.png\",\"size\":34959,\"mime\":\"\",\"assetID\":\"35\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"191\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w400.png\",\"size\":117732,\"mime\":\"\",\"assetID\":\"36\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"383\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w800.png\",\"size\":390771,\"mime\":\"\",\"assetID\":\"37\"},\"w1200hc0\":{\"w\":\"1200\",\"h\":\"575\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w1200.png\",\"size\":800902,\"mime\":\"\",\"assetID\":\"38\"},\"w1600hc0\":{\"w\":\"1600\",\"h\":\"767\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w1600.png\",\"size\":1338008,\"mime\":\"\",\"assetID\":\"39\"},\"w2000hc0\":{\"w\":\"2000\",\"h\":\"959\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w2000.png\",\"size\":1978480,\"mime\":\"\",\"assetID\":\"40\"}}},\"imagecloseup\":null,\"alt\":\"Contact D3 Creative\"}',' I welcome the opportunity to talk to you about your business Please complete all the fields below to get in touch left       Contact D3 Creative ','2015-07-08 10:24:27','1'),
	(13,1,6,1,3,1000,'{\"_id\":\"1\",\"text\":\"Manchester-based website design and development services\",\"_title\":\"Manchester-based website design and development services \\/services \\/happy-clients Girl blowing bubbles\",\"textarea\":{\"raw\":\"Mobile-friendly, content managed websites designed for business.\",\"processed\":\"<p>Mobile-friendly, content managed websites designed for business.<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Services\",\"btn-link-primary\":\"\\/services\",\"btn-txt-secondary\":\"Happy clients\",\"btn-link-secondary\":\"\\/happy-clients\",\"image\":{\"assetID\":\"1\",\"title\":\"Bubbles girl 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-girl-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-girl-3.jpg\",\"size\":129821,\"w\":1400,\"h\":550,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"58\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-girl-3-thumb@2x.jpg\",\"size\":10515,\"mime\":\"\",\"assetID\":\"2\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"78\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w200.jpg\",\"size\":5648,\"mime\":\"\",\"assetID\":\"5\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"157\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w400.jpg\",\"size\":16023,\"mime\":\"\",\"assetID\":\"6\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"314\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w800.jpg\",\"size\":48158,\"mime\":\"\",\"assetID\":\"7\"},\"w1200hc0\":{\"w\":\"1200\",\"h\":\"471\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1200.jpg\",\"size\":88792,\"mime\":\"\",\"assetID\":\"8\"},\"w1600hc0\":{\"w\":\"1400\",\"h\":\"550\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1600.jpg\",\"size\":129821,\"mime\":\"\",\"assetID\":\"9\"},\"w2000hc0\":{\"w\":\"1400\",\"h\":\"550\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w2000.jpg\",\"size\":129821,\"mime\":\"\",\"assetID\":\"10\"}}},\"imagecloseup\":{\"assetID\":\"3\",\"title\":\"Bubbles 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-3.jpg\",\"size\":42220,\"w\":800,\"h\":470,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"88\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-3-thumb@2x.jpg\",\"size\":9144,\"mime\":\"\",\"assetID\":\"4\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"117\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w200.jpg\",\"size\":5375,\"mime\":\"\",\"assetID\":\"11\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"235\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w400.jpg\",\"size\":14133,\"mime\":\"\",\"assetID\":\"12\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w800.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"13\"},\"w1200hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1200.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"14\"},\"w1600hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1600.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"15\"},\"w2000hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w2000.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"16\"}}},\"alt\":\"Girl blowing bubbles\"}',' Manchester-based website design and development services Mobile-friendly, content managed websites designed for business. left Services /services Happy clients /happy-clients   Girl blowing bubbles ','2015-07-08 10:24:56','1'),
	(14,1,6,1,4,1000,'{\"_id\":\"1\",\"text\":\"Manchester-based website design and development services\",\"_title\":\"Manchester-based website design and development services \\/services \\/happy-clients Girl blowing bubbles\",\"textarea\":{\"raw\":\"Mobile-friendly, content managed websites designed for business.\",\"processed\":\"<p>Mobile-friendly, content managed websites designed for business.<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Services\",\"btn-link-primary\":\"\\/services\",\"btn-txt-secondary\":\"Happy clients\",\"btn-link-secondary\":\"\\/happy-clients\",\"image\":{\"assetID\":\"1\",\"title\":\"Bubbles girl 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-girl-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-girl-3.jpg\",\"size\":129821,\"w\":1400,\"h\":550,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"58\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-girl-3-thumb@2x.jpg\",\"size\":10515,\"mime\":\"\",\"assetID\":\"2\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"78\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w200.jpg\",\"size\":5648,\"mime\":\"\",\"assetID\":\"5\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"157\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w400.jpg\",\"size\":16023,\"mime\":\"\",\"assetID\":\"6\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"314\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w800.jpg\",\"size\":48158,\"mime\":\"\",\"assetID\":\"7\"},\"w1200hc0\":{\"w\":\"1200\",\"h\":\"471\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1200.jpg\",\"size\":88792,\"mime\":\"\",\"assetID\":\"8\"},\"w1600hc0\":{\"w\":\"1400\",\"h\":\"550\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1600.jpg\",\"size\":129821,\"mime\":\"\",\"assetID\":\"9\"},\"w2000hc0\":{\"w\":\"1400\",\"h\":\"550\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w2000.jpg\",\"size\":129821,\"mime\":\"\",\"assetID\":\"10\"}}},\"imagecloseup\":{\"assetID\":\"3\",\"title\":\"Bubbles 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-3.jpg\",\"size\":42220,\"w\":800,\"h\":470,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"88\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-3-thumb@2x.jpg\",\"size\":9144,\"mime\":\"\",\"assetID\":\"4\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"117\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w200.jpg\",\"size\":5375,\"mime\":\"\",\"assetID\":\"11\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"235\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w400.jpg\",\"size\":14133,\"mime\":\"\",\"assetID\":\"12\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w800.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"13\"},\"w1200hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1200.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"14\"},\"w1600hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1600.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"15\"},\"w2000hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w2000.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"16\"}}},\"alt\":\"Girl blowing bubbles\"}',' Manchester-based website design and development services Mobile-friendly, content managed websites designed for business. left Services /services Happy clients /happy-clients   Girl blowing bubbles ','2015-07-08 14:21:08','1'),
	(15,1,6,1,5,1000,'{\"_id\":\"1\",\"text\":\"Manchester-based website design and development services\",\"_title\":\"Manchester-based website design and development services \\/services \\/happy-clients Girl blowing bubbles\",\"textarea\":{\"raw\":\"Mobile-friendly, content managed websites designed for business.\",\"processed\":\"<p>Mobile-friendly, content managed websites designed for business.<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Services\",\"btn-link-primary\":\"\\/services\",\"btn-txt-secondary\":\"Happy clients\",\"btn-link-secondary\":\"\\/happy-clients\",\"image\":{\"assetID\":\"1\",\"title\":\"Bubbles girl 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-girl-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-girl-3.jpg\",\"size\":129821,\"w\":1400,\"h\":550,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"58\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-girl-3-thumb@2x.jpg\",\"size\":10515,\"mime\":\"\",\"assetID\":\"2\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"78\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w200.jpg\",\"size\":5648,\"mime\":\"\",\"assetID\":\"5\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"157\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w400.jpg\",\"size\":16023,\"mime\":\"\",\"assetID\":\"6\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"314\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w800.jpg\",\"size\":48158,\"mime\":\"\",\"assetID\":\"7\"},\"w1200hc0\":{\"w\":\"1200\",\"h\":\"471\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1200.jpg\",\"size\":88792,\"mime\":\"\",\"assetID\":\"8\"},\"w1600hc0\":{\"w\":\"1400\",\"h\":\"550\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1600.jpg\",\"size\":129821,\"mime\":\"\",\"assetID\":\"9\"},\"w2000hc0\":{\"w\":\"1400\",\"h\":\"550\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w2000.jpg\",\"size\":129821,\"mime\":\"\",\"assetID\":\"10\"}}},\"imagecloseup\":{\"assetID\":\"3\",\"title\":\"Bubbles 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-3.jpg\",\"size\":42220,\"w\":800,\"h\":470,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"88\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-3-thumb@2x.jpg\",\"size\":9144,\"mime\":\"\",\"assetID\":\"4\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"117\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w200.jpg\",\"size\":5375,\"mime\":\"\",\"assetID\":\"11\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"235\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w400.jpg\",\"size\":14133,\"mime\":\"\",\"assetID\":\"12\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w800.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"13\"},\"w1200hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1200.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"14\"},\"w1600hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1600.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"15\"},\"w2000hc0\":{\"w\":\"800\",\"h\":\"470\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w2000.jpg\",\"size\":42220,\"mime\":\"\",\"assetID\":\"16\"}}},\"alt\":\"Girl blowing bubbles\"}',' Manchester-based website design and development services Mobile-friendly, content managed websites designed for business. left Services /services Happy clients /happy-clients   Girl blowing bubbles ','2015-07-08 14:27:07','1'),
	(16,2,7,4,3,1000,'{\"_id\":\"2\",\"text\":\"Websites crafted for where your customers are - everywhere.\",\"_title\":\"Websites crafted for where your customers are - everywhere. \\/happy-clients \\/contact Creating a website\",\"textarea\":{\"raw\":\"A complete website solution\",\"processed\":\"<p>A complete website solution<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Happy clients\",\"btn-link-primary\":\"\\/happy-clients\",\"btn-txt-secondary\":\"Contact us\",\"btn-link-secondary\":\"\\/contact\",\"image\":{\"assetID\":\"17\",\"title\":\"Services\",\"_default\":\"\\/perch\\/resources\\/services.png\",\"bucket\":\"default\",\"path\":\"services.png\",\"size\":1687877,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"services-thumb@2x.png\",\"size\":58154,\"mime\":\"\",\"assetID\":\"18\"},\"w200hc0\":{\"w\":\"200\",\"h\":\"95\",\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w200.png\",\"size\":27994,\"mime\":\"\",\"assetID\":\"19\"},\"w400hc0\":{\"w\":\"400\",\"h\":\"191\",\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w400.png\",\"size\":97775,\"mime\":\"\",\"assetID\":\"20\"},\"w800hc0\":{\"w\":\"800\",\"h\":\"383\",\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w800.png\",\"size\":349747,\"mime\":\"\",\"assetID\":\"21\"},\"w1200hc0\":{\"w\":\"1200\",\"h\":\"575\",\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w1200.png\",\"size\":738819,\"mime\":\"\",\"assetID\":\"22\"},\"w1600hc0\":{\"w\":\"1600\",\"h\":\"767\",\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w1600.png\",\"size\":1239582,\"mime\":\"\",\"assetID\":\"23\"},\"w2000hc0\":{\"w\":\"2000\",\"h\":\"959\",\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w2000.png\",\"size\":1821700,\"mime\":\"\",\"assetID\":\"24\"}}},\"imagecloseup\":null,\"alt\":\"Creating a website\"}',' Websites crafted for where your customers are - everywhere. A complete website solution left Happy clients /happy-clients Contact us /contact   Creating a website ','2015-07-08 15:37:58','1'),
	(17,5,21,6,0,1000,'','','2015-07-08 15:42:39',''),
	(18,5,21,6,1,1000,'{\"_id\":\"5\",\"company-name\":\"D3 Creative\",\"address-line-1\":\"Studio 40,\",\"address-line-2\":\"792 Wilmslow Road,\",\"address-line-3\":\"Didsbury,\",\"address-line-4\":\"Manchester.\",\"address-line-5\":null,\"postcode\":\"M20 6UG\",\"phone-number\":\"0161 241 8050\",\"fax-number\":null,\"mobile-number\":\"07815 105465\",\"email-address-1\":\"s.meehan@d3creative.uk\",\"media-company-name\":null,\"media-name\":null,\"media-phone-number\":null,\"email-address\":null}',' D3 Creative Studio 40, 792 Wilmslow Road, Didsbury, Manchester.  M20 6UG 0161 241 8050  07815 105465 s.meehan@d3creative.uk     ','2015-07-08 15:43:57','1'),
	(19,5,21,6,2,1000,'{\"_id\":\"5\",\"company-name\":\"D3 Creative\",\"address-line-1\":\"Studio 40,\",\"address-line-2\":\"792 Wilmslow Road,\",\"address-line-3\":\"Didsbury,\",\"address-line-4\":\"Manchester.\",\"address-line-5\":null,\"postcode\":\"M20 6UG\",\"phone-number\":\"0161 241 8050\",\"fax-number\":null,\"mobile-number\":\"07815 105465\",\"email-address-1\":\"s.meehan@d3creative.uk\",\"media-company-name\":null,\"media-name\":null,\"media-phone-number\":null,\"email-address\":null}',' D3 Creative Studio 40, 792 Wilmslow Road, Didsbury, Manchester.  M20 6UG 0161 241 8050  07815 105465 s.meehan@d3creative.uk     ','2015-07-08 15:44:37','1'),
	(39,6,14,6,19,1000,'{\"_id\":\"6\",\"form-intro\":{\"raw\":\"A member of our team will endeavour to contact you as soon as possible.\",\"processed\":\"<p>A member of our team will endeavour to contact you as soon as possible.<\\/p>\"},\"smallprint\":{\"raw\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\",\"processed\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\"},\"success\":{\"raw\":\"Thank you for taking the time to complete this form.\",\"processed\":\"<p>Thank you for taking the time to complete this form.<\\/p>\"},\"company-name\":null,\"address-line-1\":null,\"address-line-2\":null,\"address-line-3\":null,\"address-line-4\":null,\"address-line-5\":null,\"postcode\":null,\"phone-number\":null,\"fax-number\":null,\"mobile-number\":null,\"email-address-1\":null,\"media-company-name\":null,\"media-name\":null,\"media-phone-number\":null,\"email-address\":null}',' A member of our team will endeavour to contact you as soon as possible. Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties. Thank you for taking the time to complete this form.                ','2015-07-08 16:28:33','1'),
	(40,6,14,6,20,1000,'{\"_id\":\"6\",\"form-intro\":{\"raw\":\"A member of our team will endeavour to contact you as soon as possible.\",\"processed\":\"<p>A member of our team will endeavour to contact you as soon as possible.<\\/p>\"},\"smallprint\":{\"raw\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\",\"processed\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\"},\"success\":{\"raw\":\"Thank you for taking the time to complete this form.\",\"processed\":\"<p>Thank you for taking the time to complete this form.<\\/p>\"}}',' A member of our team will endeavour to contact you as soon as possible. Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties. Thank you for taking the time to complete this form. ','2015-07-08 16:33:25','1'),
	(45,6,14,6,22,1000,'{\"_id\":\"6\",\"form-intro\":{\"raw\":\"Really great communication is the key to a successful project, let\\u2019s start as we mean to go on:\",\"processed\":\"<p>Really great communication is the key to a successful project, let\\u2019s start as we mean to go on:<\\/p>\"},\"smallprint\":{\"raw\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\",\"processed\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\"},\"success\":{\"raw\":\"Thank you for taking the time to complete this form.\",\"processed\":\"<p>Thank you for taking the time to complete this form.<\\/p>\"}}',' Really great communication is the key to a successful project, let’s start as we mean to go on: Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties. Thank you for taking the time to complete this form. ','2015-07-08 16:50:49','1'),
	(42,5,21,6,4,1000,'{\"_id\":\"5\",\"company-name\":\"D3 Creative\",\"address-line-1\":\"Studio 40,\",\"address-line-2\":\"792 Wilmslow Road,\",\"address-line-3\":\"Didsbury,\",\"address-line-4\":\"Manchester.\",\"address-line-5\":null,\"postcode\":\"M20 6UG\",\"phone-number\":\"0161 241 8050\",\"fax-number\":null,\"mobile-number\":\"07815 105465\",\"email-address-1\":\"s.meehan@d3creative.uk\",\"media-company-name\":null,\"media-name\":null,\"media-phone-number\":null,\"email-address\":null}',' D3 Creative Studio 40, 792 Wilmslow Road, Didsbury, Manchester.  M20 6UG 0161 241 8050  07815 105465 s.meehan@d3creative.uk     ','2015-07-08 16:34:44','1'),
	(43,5,21,6,5,1000,'{\"_id\":\"5\",\"company-name\":\"D3 Creative\",\"address-line-1\":\"Studio 40,\",\"address-line-2\":\"792 Wilmslow Road,\",\"address-line-3\":\"Didsbury,\",\"address-line-4\":\"Manchester.\",\"address-line-5\":null,\"postcode\":\"M20 6UG\",\"phone-number\":\"0161 241 8050\",\"fax-number\":null,\"mobile-number\":\"07815 105465\",\"email-address-1\":\"s.meehan@d3creative.uk\",\"media-company-name\":null,\"media-name\":null,\"media-phone-number\":null,\"email-address\":null}',' D3 Creative Studio 40, 792 Wilmslow Road, Didsbury, Manchester.  M20 6UG 0161 241 8050  07815 105465 s.meehan@d3creative.uk     ','2015-07-08 16:50:26','1'),
	(46,5,21,6,6,1000,'{\"_id\":\"5\",\"company-name\":\"D3 Creative\",\"address-line-1\":\"Studio 40,\",\"address-line-2\":\"792 Wilmslow Road,\",\"address-line-3\":\"Didsbury,\",\"address-line-4\":\"Manchester.\",\"address-line-5\":null,\"postcode\":\"M20 6UG\",\"phone-number\":\"0161 241 8050\",\"fax-number\":null,\"mobile-number\":\"07815 105465\",\"email-address-1\":\"s.meehan@d3creative.uk\",\"media-company-name\":null,\"media-name\":null,\"media-phone-number\":null,\"email-address\":null}',' D3 Creative Studio 40, 792 Wilmslow Road, Didsbury, Manchester.  M20 6UG 0161 241 8050  07815 105465 s.meehan@d3creative.uk     ','2015-07-08 16:58:42','1'),
	(47,6,14,6,23,1000,'{\"_id\":\"6\",\"form-intro\":{\"raw\":\"Really great communication is the key to a successful project, let\\u2019s start as we mean to go on:\",\"processed\":\"<p>Really great communication is the key to a successful project, let\\u2019s start as we mean to go on:<\\/p>\"},\"smallprint\":{\"raw\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\",\"processed\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\"},\"success\":{\"raw\":\"Thank you for taking the time to complete this form.\",\"processed\":\"<p>Thank you for taking the time to complete this form.<\\/p>\"}}',' Really great communication is the key to a successful project, let’s start as we mean to go on: Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties. Thank you for taking the time to complete this form. ','2015-07-08 17:01:23','1'),
	(57,7,2,1,9,1000,'','','2015-07-08 17:17:27','1'),
	(58,7,2,1,10,1000,'','','2015-07-08 17:30:53','1'),
	(51,7,2,1,3,1000,'','','2015-07-08 17:08:41','1'),
	(52,7,2,1,4,1000,'','','2015-07-08 17:09:16','1'),
	(53,7,2,1,5,1000,'','','2015-07-08 17:09:31','1'),
	(54,7,2,1,6,1000,'','','2015-07-08 17:09:49','1'),
	(55,7,2,1,7,1000,'','','2015-07-08 17:10:11','1'),
	(56,7,2,1,8,1000,'','','2015-07-08 17:12:06','1'),
	(36,6,14,6,16,1000,'{\"_id\":\"6\",\"form-intro\":{\"raw\":\"A member of our team will endeavour to contact you as soon as possible.\",\"processed\":\"<p>A member of our team will endeavour to contact you as soon as possible.<\\/p>\"},\"smallprint\":{\"raw\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\",\"processed\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\"},\"success\":{\"raw\":\"Thank you for taking the time to complete this form.\",\"processed\":\"<p>Thank you for taking the time to complete this form.<\\/p>\"}}',' A member of our team will endeavour to contact you as soon as possible. Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties. Thank you for taking the time to complete this form. ','2015-07-08 16:04:06','1'),
	(37,6,14,6,17,1000,'{\"_id\":\"6\",\"form-intro\":{\"raw\":\"A member of our team will endeavour to contact you as soon as possible.\",\"processed\":\"<p>A member of our team will endeavour to contact you as soon as possible.<\\/p>\"},\"smallprint\":{\"raw\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\",\"processed\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\"},\"success\":{\"raw\":\"Thank you for taking the time to complete this form.\",\"processed\":\"<p>Thank you for taking the time to complete this form.<\\/p>\"}}',' A member of our team will endeavour to contact you as soon as possible. Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties. Thank you for taking the time to complete this form. ','2015-07-08 16:04:24','1'),
	(38,6,14,6,18,1000,'{\"_id\":\"6\",\"form-intro\":{\"raw\":\"A member of our team will endeavour to contact you as soon as possible.\",\"processed\":\"<p>A member of our team will endeavour to contact you as soon as possible.<\\/p>\"},\"smallprint\":{\"raw\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\",\"processed\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\"},\"success\":{\"raw\":\"Thank you for taking the time to complete this form.\",\"processed\":\"<p>Thank you for taking the time to complete this form.<\\/p>\"},\"company-name\":null,\"address-line-1\":null,\"address-line-2\":null,\"address-line-3\":null,\"address-line-4\":null,\"address-line-5\":null,\"postcode\":null,\"phone-number\":null,\"fax-number\":null,\"mobile-number\":null,\"email-address-1\":null,\"media-company-name\":null,\"media-name\":null,\"media-phone-number\":null,\"email-address\":null}',' A member of our team will endeavour to contact you as soon as possible. Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties. Thank you for taking the time to complete this form.                ','2015-07-08 16:06:20','1'),
	(41,5,21,6,3,1000,'{\"_id\":\"5\",\"company-name\":\"D3 Creative WOO\",\"address-line-1\":\"Studio 40,\",\"address-line-2\":\"792 Wilmslow Road,\",\"address-line-3\":\"Didsbury,\",\"address-line-4\":\"Manchester.\",\"address-line-5\":null,\"postcode\":\"M20 6UG\",\"phone-number\":\"0161 241 8050\",\"fax-number\":null,\"mobile-number\":\"07815 105465\",\"email-address-1\":\"s.meehan@d3creative.uk\",\"media-company-name\":null,\"media-name\":null,\"media-phone-number\":null,\"email-address\":null}',' D3 Creative WOO Studio 40, 792 Wilmslow Road, Didsbury, Manchester.  M20 6UG 0161 241 8050  07815 105465 s.meehan@d3creative.uk     ','2015-07-08 16:34:30','1'),
	(44,6,14,6,21,1000,'{\"_id\":\"6\",\"form-intro\":{\"raw\":\"A member of our team will endeavour to contact you as soon as possible.\",\"processed\":\"<p>A member of our team will endeavour to contact you as soon as possible.<\\/p>\"},\"smallprint\":{\"raw\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\",\"processed\":\"Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.\"},\"success\":{\"raw\":\"Thank you for taking the time to complete this form.\",\"processed\":\"<p>Thank you for taking the time to complete this form.<\\/p>\"}}',' A member of our team will endeavour to contact you as soon as possible. Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties. Thank you for taking the time to complete this form. ','2015-07-08 16:50:32','1');

/*!40000 ALTER TABLE `perch2_content_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_content_regions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_content_regions`;

CREATE TABLE `perch2_content_regions` (
  `regionID` int(10) NOT NULL AUTO_INCREMENT,
  `pageID` int(10) unsigned NOT NULL,
  `regionKey` varchar(255) NOT NULL DEFAULT '',
  `regionPage` varchar(255) NOT NULL DEFAULT '',
  `regionHTML` longtext NOT NULL,
  `regionNew` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `regionOrder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `regionTemplate` varchar(255) NOT NULL DEFAULT '',
  `regionMultiple` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `regionOptions` text NOT NULL,
  `regionSearchable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `regionRev` int(10) unsigned NOT NULL DEFAULT '0',
  `regionLatestRev` int(10) unsigned NOT NULL DEFAULT '0',
  `regionEditRoles` varchar(255) NOT NULL DEFAULT '*',
  `regionUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`regionID`),
  KEY `idx_key` (`regionKey`),
  KEY `idx_path` (`regionPage`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_content_regions` WRITE;
/*!40000 ALTER TABLE `perch2_content_regions` DISABLE KEYS */;

INSERT INTO `perch2_content_regions` (`regionID`, `pageID`, `regionKey`, `regionPage`, `regionHTML`, `regionNew`, `regionOrder`, `regionTemplate`, `regionMultiple`, `regionOptions`, `regionSearchable`, `regionRev`, `regionLatestRev`, `regionEditRoles`, `regionUpdated`)
VALUES
	(16,1,'CTA Large','/','<!-- Undefined content: CTA Large -->',1,2,'',0,'',1,0,0,'*','2015-07-07 14:49:51'),
	(2,1,'Intro','/','<div class=\"row home-intro\">\n    <div class=\"inner-container two-col-absolute\">\n        <div class=\"main\">\n            <div class=\"pad\">\n                <p>Since 2005 I’ve worked with businesses of all sizes to plan, prototype, design and develop content managed websites.</p>\n                <p>I’m passionate about my work, there’s always something new to learn. I bring this energy and my years of experience to every project I’m involved with.</p>\n                <small>Stephen Meehan - D3 Creative</small></div>\n        </div><!-- Main -->\n        <aside>\n            <div class=\"pad\">\n                <h3>All websites feature:</h3>\n                <ul class=\"with-icon\">\n                    <li><i class=\"fa fa-check\"></i>Websites work on mobile, tablet and desktop devices </li>\n                    <li><i class=\"fa fa-check\"></i> Clear step-by-step tutorials explain how to use the CMS</li>\n                    <li><i class=\"fa fa-check\"></i> CMS is easy to manage and control by non-technical users </li>\n                </ul>\n            </div><!-- pad -->\n        </aside>\n    </div><!-- inner-container -->\n</div><!-- row -->\n\n\n\n\n',0,1,'custom/home-intro.html',0,'{\"edit_mode\":\"singlepage\"}',1,10,10,'*','2015-07-08 17:30:53'),
	(3,3,'Error description','/errors/404','<!-- Undefined content: Error description -->',1,0,'',0,'',1,0,0,'*','2015-07-03 13:47:04'),
	(15,1,'Main','/','<!-- Undefined content: Main -->',1,1,'',0,'',1,0,0,'*','2015-07-07 14:49:51'),
	(11,5,'Hero','/happy-clients','<div class=\"row hero\"> \n    <div class=\"inner-container text-align left\">\n        <h1>Making clients happy since 2005 we strive to be the best</h1>\n        <p>We design, develop and deliver</p>\n       \n\n        <a class=\"btn primary\" title=\"Services\" href=\"/services\">Services</a><a class=\"btn secondary\" title=\"Contact us\" href=\"/contact\">Contact us</a>\n\n  </div>\n    <div class=\"wrap\">\n      \n       <picture>\n    <!--[if IE 9]><video style=\"display: none;\"><![endif]-->\n    <source\n    \n    sizes=\"100vw\"\n    srcset=\"/perch/resources/happy-clients-w200.png 200w,   \n            /perch/resources/happy-clients-w400.png 400w,\n            /perch/resources/happy-clients-w800.png 800w,\n            /perch/resources/happy-clients-w1200.png 1200w,\n            /perch/resources/happy-clients-w1600.png 1600w,\n            /perch/resources/happy-clients-w2000.png 2000w\"\n    >\n\n  <!--[if IE 9]></video><![endif]-->\n    <img\n    src=\"/perch/resources/happy-clients-w2000.png\" alt=\"Happy client\">\n\n</picture>\n\n\n\n\n\n\n\n\n\n    </div>\n</div>\n\n\n\n\n\n\n\n',0,0,'blocks/hero-jumbo.html',0,'{\"edit_mode\":\"singlepage\"}',1,2,2,'*','2015-07-08 10:24:19'),
	(6,1,'Hero','/','<div class=\"row hero\"> \n    <div class=\"inner-container text-align left\">\n        <h1>Manchester-based website design and development services</h1>\n        <p>Mobile-friendly, content managed websites designed for business.</p>\n       \n\n        <a class=\"btn primary\" title=\"Services\" href=\"/services\">Services</a><a class=\"btn secondary\" title=\"Happy clients\" href=\"/happy-clients\">Happy clients</a>\n\n  </div>\n    <div class=\"wrap\">\n      \n       <picture>\n    <!--[if IE 9]><video style=\"display: none;\"><![endif]-->\n    <source\n    media=\"(min-width: 800px)\"\n    sizes=\"100vw\"\n    srcset=\"/perch/resources/bubbles-girl-3-w200.jpg 200w,   \n            /perch/resources/bubbles-girl-3-w400.jpg 400w,\n            /perch/resources/bubbles-girl-3-w800.jpg 800w,\n            /perch/resources/bubbles-girl-3-w1200.jpg 1200w,\n            /perch/resources/bubbles-girl-3-w1600.jpg 1600w,\n            /perch/resources/bubbles-girl-3-w2000.jpg 2000w\"\n    >\n\n  \n    <source\n    media=\"(max-width: 799px)\"\n    sizes=\"100vw\"\n    srcset=\"/perch/resources/bubbles-3-w200.jpg 200w,\n            /perch/resources/bubbles-3-w400.jpg 400w,\n            /perch/resources/bubbles-3-w800.jpg 800w,\n            /perch/resources/bubbles-3-w1200.jpg 1200w,\n            /perch/resources/bubbles-3-w1600.jpg 1600w,\n            /perch/resources/bubbles-3-w2000.jpg 2000w\"\n    ><!--[if IE 9]></video><![endif]-->\n    <img\n    src=\"/perch/resources/bubbles-girl-3-w2000.jpg\" alt=\"Girl blowing bubbles\">\n\n</picture>\n</div>\n</div>\n\n\n\n\n\n\n\n',0,0,'blocks/hero-jumbo.html',0,'{\"edit_mode\":\"singlepage\"}',1,5,5,'*','2015-07-08 14:27:07'),
	(7,4,'Hero','/services','<div class=\"row hero\"> \n    <div class=\"inner-container text-align left\">\n        <h1>Websites crafted for where your customers are - everywhere.</h1>\n        <p>A complete website solution</p>\n       \n\n        <a class=\"btn primary\" title=\"Happy clients\" href=\"/happy-clients\">Happy clients</a><a class=\"btn secondary\" title=\"Contact us\" href=\"/contact\">Contact us</a>\n\n  </div>\n    <div class=\"wrap\">\n      \n       <picture>\n    <!--[if IE 9]><video style=\"display: none;\"><![endif]-->\n    <source\n    \n    sizes=\"100vw\"\n    srcset=\"/perch/resources/services-w200.png 200w,   \n            /perch/resources/services-w400.png 400w,\n            /perch/resources/services-w800.png 800w,\n            /perch/resources/services-w1200.png 1200w,\n            /perch/resources/services-w1600.png 1600w,\n            /perch/resources/services-w2000.png 2000w\"\n    >\n\n  <!--[if IE 9]></video><![endif]-->\n    <img\n    src=\"/perch/resources/services-w2000.png\" alt=\"Creating a website\">\n\n</picture>\n</div>\n</div>\n\n\n\n\n\n\n\n',0,0,'blocks/hero-jumbo.html',0,'{\"edit_mode\":\"singlepage\"}',1,3,3,'*','2015-07-08 15:37:58'),
	(9,1,'Latest project','/','<!-- Undefined content: Latest project -->',1,2,'',0,'',1,0,0,'*','2015-07-03 13:56:58'),
	(10,4,'Main','/services','<!-- Undefined content: Main -->',1,1,'',0,'',1,0,0,'*','2015-07-03 13:57:02'),
	(12,5,'Main','/happy-clients','<!-- Undefined content: Main -->',1,1,'',0,'',1,0,0,'*','2015-07-03 13:57:40'),
	(13,6,'Hero','/contact','<div class=\"row hero\"> \n    <div class=\"inner-container text-align left\">\n        <h1>I welcome the opportunity to talk to you about your business</h1>\n        <p>Please complete all the fields below to get in touch</p>\n       \n\n        \n\n  </div>\n    <div class=\"wrap\">\n      \n       <picture>\n    <!--[if IE 9]><video style=\"display: none;\"><![endif]-->\n    <source\n    \n    sizes=\"100vw\"\n    srcset=\"/perch/resources/contact-w200.png 200w,   \n            /perch/resources/contact-w400.png 400w,\n            /perch/resources/contact-w800.png 800w,\n            /perch/resources/contact-w1200.png 1200w,\n            /perch/resources/contact-w1600.png 1600w,\n            /perch/resources/contact-w2000.png 2000w\"\n    >\n\n  <!--[if IE 9]></video><![endif]-->\n    <img\n    src=\"/perch/resources/contact-w2000.png\" alt=\"Contact D3 Creative\">\n\n</picture>\n\n\n\n\n\n\n\n\n\n    </div>\n</div>\n\n\n\n\n\n\n\n',0,0,'blocks/hero-jumbo.html',0,'{\"edit_mode\":\"singlepage\"}',1,2,2,'*','2015-07-08 10:24:27'),
	(14,6,'Main','/contact','<div class=\"row\">\n	<div class=\"inner-container two-col-absolute\">\n		<div class=\"main\"><perch:form template=\"/templates/content/custom/contact-page.html\" novalidate=\"novalidate\" id=\"company-contact-form\" method=\"post\" app=\"perch_forms\">\n\n\n\n\n\n<div class=\"pad\">\n\n<p>Really great communication is the key to a successful project, let’s start as we mean to go on:</p>\n\n\n<perch:error for=\"full-name\" type=\"required\">\n            <p class=\"error\">\n                Please add your name\n            </p>\n        </perch:error>\n        <perch:error for=\"company-name\" type=\"required\">\n            <p class=\"error\">\n                Please add company name\n            </p>\n        </perch:error>\n        <perch:error for=\"telephone\" type=\"required\">\n            <p class=\"error\">\n                Please add your telephone number\n            </p>\n        </perch:error>\n        <perch:error for=\"email-address\" type=\"required\">\n                <p class=\"error\">\n                    Please add your email address\n                </p>\n            </perch:error>\n            <perch:error for=\"email-address\" type=\"format\">\n                <p class=\"error\">\n                    Please check your email address\n                </p>\n            </perch:error>\n            <perch:error for=\"phone-number\" type=\"required\">\n                    <p class=\"error\">\n                        Please add your phone number\n                    </p>\n                </perch:error>\n                <perch:error for=\"enquiry\" type=\"required\">\n            <p class=\"error\">\n                Please write a message\n            </p>\n        </perch:error>\n\n    \n        \n        <div class=\"row\">\n\n        <div class=\"inline\">\n        <perch:label for=\"full-name\">Full name</perch:label>\n        <perch:error for=\"full-name\" type=\"required\"><p class=\"error\">Please add your name</p></perch:error>                        \n        <perch:input id=\"full-name\" name=\"full-name\" type=\"text\" class=\"required\" title=\"Full name. This is a required field\" required=\"true\" label=\"Name\"/>\n        <small class=\"hint\">First name and surname</small>\n        </div>\n\n        <div class=\"inline last\">\n        <perch:label for=\"company-name\">Company name</perch:label>\n        <perch:error for=\"company-name\" type=\"required\"><p class=\"error\">Please add company name</p></perch:error>                      \n        <perch:input id=\"company-name\" name=\"company-name\" type=\"text\" class=\"required\" title=\"Company name. This is a required field\" required=\"true\" label=\"Company name\"/>\n        \n        \n        </div>\n\n        </div><!-- row -->\n        <div class=\"row\">\n\n        <div class=\"inline\">\n        <perch:label for=\"telephone\">Telephone</perch:label>\n        <perch:error for=\"telephone\" type=\"required\"><p class=\"error\">Please add your telephone number</p></perch:error>                        \n        <perch:input id=\"telephone\" name=\"telephone\" type=\"text\" class=\"required\" title=\"Telephone. This is a required field\" required=\"true\" label=\"Telephone\"/>\n        <small class=\"hint\">Business telephone number</small>\n        </div>\n\n        <div class=\"inline last\">\n        <perch:label for=\"email-address\">Email address</perch:label>\n        <perch:error for=\"email-address\" type=\"required\"><p class=\"error\">Please add your email address</p></perch:error>\n        <perch:error for=\"email-address\" type=\"format\"><p class=\"error\">Please check your email address</p></perch:error>\n        <perch:input id=\"email-address\" name=\"email-address\" type=\"email\" class=\"required\" title=\"Email address. This is a required field\" required=\"true\" label=\"Email\" />\n         <small class=\"hint\">Business email address</small>\n        </div>\n\n        </div><!-- row -->\n\n\n\n        <div class=\"row\">\n        \n        <perch:label for=\"enquiry\">Message</perch:label>\n        <perch:error for=\"enquiry\" type=\"required\"><p class=\"error\">Please write a message</p></perch:error>\n        <perch:input type=\"textarea\" id=\"enquiry\" required=\"true\" label=\"Enquiry\" cols=\"20\" rows=\"3\" class=\"required\" title=\"Message\"/>\n        <small class=\"hint\">How can we help?</small>\n        \n        \n\n        </div><!-- row -->\n            \n   <div class=\"row\">\n<small>Your personal details will be held securely by D3 Creative in accordance with the Data Protection Act 2000, and will be used solely in connection with this website and will not be passed to any third parties.</small>\n</div>\n\n  <div class=\"row\">\n<perch:input class=\"btn submit\" title=\"Submit form\" type=\"submit\" id=\"submit\" value=\"Send message\"/>\n</div>\n\n</div>    \n        <perch:success>\n        \n        <div class=\"pad\"><p>Thank you for taking the time to complete this form.</p></div>\n        </perch:success>\n    \n</perch:form>\n   \n</div>\n\n		\n\n\n',0,1,'custom/contact-page.html',0,'{\"edit_mode\":\"listdetail\",\"searchURL\":\"\",\"title_delimit\":\"\",\"adminOnly\":0,\"addToTop\":0,\"limit\":false}',1,23,23,'*','2015-07-08 17:01:23'),
	(17,5,'CTA Large','/happy-clients','<!-- Undefined content: CTA Large -->',1,2,'',0,'',1,0,0,'*','2015-07-07 15:09:42'),
	(18,6,'CTA Large','/contact','<!-- Undefined content: CTA Large -->',1,3,'',0,'',1,0,0,'*','2015-07-08 15:42:31'),
	(19,4,'CTA Large','/services','<!-- Undefined content: CTA Large -->',1,3,'',0,'',1,0,0,'*','2015-07-08 15:42:31'),
	(21,6,'Contact details','*','<div class=\"pad contact-details\">\n<ul class=\"stacked list\">\n<li><strong class=\"company-name\">D3 Creative</strong></li>\n<li>Studio 40,</li>\n<li>792 Wilmslow Road,</li>\n<li>Didsbury,</li>\n<li>Manchester.</li>\n\n<li>M20 6UG</li>\n</ul>\n\n<ul class=\"stacked list numbers\">\n\n<li><strong>T:</strong><a class=\"digits telephone\" title=\"0161 241 8050\" href=\"tel:0161%20241%208050\">0161 241 8050\n</li></a>\n\n\n<li><strong>M:</strong><a class=\"digits mobile\" title=\"07815 105465\" href=\"tel:\"07815%20105465\">07815 105465\n</li></a>\n<li><strong>E:</strong><a class=\"email\" title=\"s.meehan@d3creative.uk\" href=\"mailto:s.meehan@d3creative.uk\">s.meehan@d3creative.uk</a></li>\n\n</ul>\n\n\n</div>\n\n\n\n\n\n\n\n',0,2,'custom/contact-details.html',0,'{\"edit_mode\":\"listdetail\",\"searchURL\":\"\",\"title_delimit\":\"\",\"adminOnly\":0,\"addToTop\":0,\"limit\":false}',1,6,6,'*','2015-07-08 16:58:42');

/*!40000 ALTER TABLE `perch2_content_regions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_navigation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_navigation`;

CREATE TABLE `perch2_navigation` (
  `groupID` int(10) NOT NULL AUTO_INCREMENT,
  `groupTitle` varchar(255) NOT NULL DEFAULT '',
  `groupSlug` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_navigation_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_navigation_pages`;

CREATE TABLE `perch2_navigation_pages` (
  `navpageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageID` int(10) unsigned NOT NULL DEFAULT '0',
  `groupID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageOrder` int(10) unsigned NOT NULL DEFAULT '1',
  `pageDepth` tinyint(10) unsigned NOT NULL,
  `pageTreePosition` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`navpageID`),
  KEY `idx_group` (`groupID`),
  KEY `idx_page_group` (`pageID`,`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_page_routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_page_routes`;

CREATE TABLE `perch2_page_routes` (
  `routeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageID` int(10) unsigned NOT NULL DEFAULT '0',
  `routePattern` char(255) NOT NULL DEFAULT '',
  `routeRegExp` char(255) NOT NULL DEFAULT '',
  `routeOrder` int(10) unsigned NOT NULL,
  PRIMARY KEY (`routeID`),
  KEY `idx_page` (`pageID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_page_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_page_templates`;

CREATE TABLE `perch2_page_templates` (
  `templateID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `templateTitle` varchar(255) NOT NULL DEFAULT '',
  `templatePath` varchar(255) NOT NULL DEFAULT '',
  `optionsPageID` int(10) unsigned NOT NULL DEFAULT '0',
  `templateReference` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `templateNavGroups` varchar(255) DEFAULT '',
  PRIMARY KEY (`templateID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_page_templates` WRITE;
/*!40000 ALTER TABLE `perch2_page_templates` DISABLE KEYS */;

INSERT INTO `perch2_page_templates` (`templateID`, `templateTitle`, `templatePath`, `optionsPageID`, `templateReference`, `templateNavGroups`)
VALUES
	(1,'Default','default.php',0,1,''),
	(2,'Home','home.php',0,1,''),
	(3,'Search','search.php',0,1,''),
	(4,'404','errors/404.php',0,1,''),
	(8,'Two - Col','two-col.php',0,1,'');

/*!40000 ALTER TABLE `perch2_page_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_pages`;

CREATE TABLE `perch2_pages` (
  `pageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `pagePath` varchar(255) NOT NULL DEFAULT '',
  `pageTitle` varchar(255) NOT NULL DEFAULT '',
  `pageNavText` varchar(255) NOT NULL DEFAULT '',
  `pageNew` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `pageOrder` int(10) unsigned NOT NULL DEFAULT '1',
  `pageDepth` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `pageSortPath` varchar(255) NOT NULL DEFAULT '',
  `pageTreePosition` varchar(64) NOT NULL DEFAULT '',
  `pageSubpageRoles` varchar(255) NOT NULL DEFAULT '',
  `pageSubpagePath` varchar(255) NOT NULL DEFAULT '',
  `pageHidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pageNavOnly` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pageAccessTags` varchar(255) NOT NULL DEFAULT '',
  `pageCreatorID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageModified` datetime NOT NULL DEFAULT '2014-01-01 00:00:00',
  `pageAttributes` text NOT NULL,
  `pageAttributeTemplate` varchar(255) NOT NULL DEFAULT 'default.html',
  `pageTemplate` char(255) NOT NULL DEFAULT '',
  `templateID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageSubpageTemplates` varchar(255) NOT NULL DEFAULT '',
  `pageCollections` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`pageID`),
  KEY `idx_parent` (`pageParentID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_pages` WRITE;
/*!40000 ALTER TABLE `perch2_pages` DISABLE KEYS */;

INSERT INTO `perch2_pages` (`pageID`, `pageParentID`, `pagePath`, `pageTitle`, `pageNavText`, `pageNew`, `pageOrder`, `pageDepth`, `pageSortPath`, `pageTreePosition`, `pageSubpageRoles`, `pageSubpagePath`, `pageHidden`, `pageNavOnly`, `pageAccessTags`, `pageCreatorID`, `pageModified`, `pageAttributes`, `pageAttributeTemplate`, `pageTemplate`, `templateID`, `pageSubpageTemplates`, `pageCollections`)
VALUES
	(1,0,'/','Home','Home',0,1,1,'/','000-001','','',0,0,'',1,'2015-07-08 16:30:53','{\"description\":{\"raw\":\"\",\"processed\":\"\"},\"keywords\":{\"raw\":\"\",\"processed\":\"\"},\"noindex\":null,\"nofollow\":null,\"nosnippet\":null}','default.html','home.php',2,'',''),
	(2,0,'/errors','Errors','Errors',0,2,1,'/errors','000-002','','',1,0,'',1,'2015-07-03 12:37:14','','default.html','errors/404.php',4,'',''),
	(3,2,'/errors/404','404','404',0,1,2,'/errors/404','000-002-001','','',1,0,'',1,'2015-07-03 12:37:14','','default.html','errors/404.php',4,'',''),
	(4,0,'/services','Services','Services',0,3,1,'/services','000-004','','',0,0,'',1,'2015-07-08 14:37:58','','default.html','default.php',1,'',''),
	(5,0,'/happy-clients','Happy clients','Happy clients',0,4,1,'/happy-clients','000-005','','',0,0,'',1,'2015-07-08 09:24:19','','default.html','default.php',1,'',''),
	(6,0,'/contact','Contact','Contact',0,5,1,'/contact','000-006','','',0,0,'',1,'2015-07-08 16:01:23','','default.html','default.php',1,'','');

/*!40000 ALTER TABLE `perch2_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_resource_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_resource_log`;

CREATE TABLE `perch2_resource_log` (
  `logID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `appID` char(32) NOT NULL DEFAULT 'content',
  `itemFK` char(32) NOT NULL DEFAULT 'itemRowID',
  `itemRowID` int(10) unsigned NOT NULL DEFAULT '0',
  `resourceID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`logID`),
  UNIQUE KEY `idx_uni` (`appID`,`itemFK`,`itemRowID`,`resourceID`),
  KEY `idx_resource` (`resourceID`),
  KEY `idx_fk` (`itemFK`,`itemRowID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_resource_log` WRITE;
/*!40000 ALTER TABLE `perch2_resource_log` DISABLE KEYS */;

INSERT INTO `perch2_resource_log` (`logID`, `appID`, `itemFK`, `itemRowID`, `resourceID`)
VALUES
	(1,'perch_core','recovery',1,1),
	(2,'perch_core','recovery',1,2),
	(3,'perch_core','recovery',1,3),
	(4,'perch_core','recovery',1,4),
	(5,'perch_core','recovery',1,5),
	(6,'perch_core','recovery',1,6),
	(7,'perch_core','recovery',1,7),
	(8,'perch_core','recovery',1,8),
	(9,'perch_core','recovery',1,9),
	(10,'perch_core','recovery',1,10),
	(11,'perch_core','recovery',1,11),
	(12,'perch_core','recovery',1,12),
	(13,'perch_core','recovery',1,13),
	(14,'perch_core','recovery',1,14),
	(15,'perch_core','recovery',1,15),
	(16,'perch_core','recovery',1,16),
	(17,'perch_core','recovery',1,17),
	(18,'perch_core','recovery',1,18),
	(19,'perch_core','recovery',1,19),
	(20,'perch_core','recovery',1,20),
	(21,'perch_core','recovery',1,21),
	(22,'perch_core','recovery',1,22),
	(23,'perch_core','recovery',1,23),
	(24,'perch_core','recovery',1,24),
	(25,'perch_core','recovery',1,25),
	(26,'perch_core','recovery',1,26),
	(27,'perch_core','recovery',1,27),
	(28,'perch_core','recovery',1,28),
	(29,'perch_core','recovery',1,29),
	(30,'perch_core','recovery',1,30),
	(31,'perch_core','recovery',1,31),
	(32,'perch_core','recovery',1,32),
	(33,'perch_core','recovery',1,33),
	(34,'perch_core','recovery',1,34),
	(35,'perch_core','recovery',1,35),
	(36,'perch_core','recovery',1,36),
	(37,'perch_core','recovery',1,37),
	(38,'perch_core','recovery',1,38),
	(39,'perch_core','recovery',1,39),
	(40,'perch_core','recovery',1,40);

/*!40000 ALTER TABLE `perch2_resource_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_resource_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_resource_tags`;

CREATE TABLE `perch2_resource_tags` (
  `tagID` int(10) NOT NULL AUTO_INCREMENT,
  `tagTitle` varchar(255) NOT NULL DEFAULT '',
  `tagSlug` varchar(255) NOT NULL DEFAULT '',
  `tagCount` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tagID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



# Dump of table perch2_resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_resources`;

CREATE TABLE `perch2_resources` (
  `resourceID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resourceApp` char(32) NOT NULL DEFAULT 'content',
  `resourceBucket` char(16) NOT NULL DEFAULT 'default',
  `resourceFile` char(255) NOT NULL DEFAULT '',
  `resourceKey` enum('orig','thumb') DEFAULT NULL,
  `resourceParentID` int(10) NOT NULL DEFAULT '0',
  `resourceType` char(4) NOT NULL DEFAULT '',
  `resourceCreated` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `resourceUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `resourceAWOL` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `resourceTitle` char(255) DEFAULT NULL,
  `resourceFileSize` int(10) unsigned DEFAULT NULL,
  `resourceWidth` int(10) unsigned DEFAULT NULL,
  `resourceHeight` int(10) unsigned DEFAULT NULL,
  `resourceCrop` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `resourceDensity` float NOT NULL DEFAULT '1',
  `resourceTargetWidth` int(10) unsigned DEFAULT NULL,
  `resourceTargetHeight` int(10) unsigned DEFAULT NULL,
  `resourceMimeType` char(64) DEFAULT NULL,
  `resourceInLibrary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`resourceID`),
  UNIQUE KEY `idx_file` (`resourceBucket`,`resourceFile`),
  KEY `idx_app` (`resourceApp`),
  KEY `idx_key` (`resourceKey`),
  KEY `idx_type` (`resourceType`),
  KEY `idx_awol` (`resourceAWOL`),
  KEY `idx_library` (`resourceInLibrary`),
  FULLTEXT KEY `idx_search` (`resourceTitle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_resources` WRITE;
/*!40000 ALTER TABLE `perch2_resources` DISABLE KEYS */;

INSERT INTO `perch2_resources` (`resourceID`, `resourceApp`, `resourceBucket`, `resourceFile`, `resourceKey`, `resourceParentID`, `resourceType`, `resourceCreated`, `resourceUpdated`, `resourceAWOL`, `resourceTitle`, `resourceFileSize`, `resourceWidth`, `resourceHeight`, `resourceCrop`, `resourceDensity`, `resourceTargetWidth`, `resourceTargetHeight`, `resourceMimeType`, `resourceInLibrary`)
VALUES
	(1,'content','default','bubbles-girl-3.jpg','orig',0,'jpg','2015-07-07 13:52:54','2015-07-07 14:52:54',0,'Bubbles girl 3',129821,1400,550,0,1,NULL,NULL,'image/jpeg',0),
	(2,'content','default','bubbles-girl-3-thumb@2x.jpg','thumb',1,'jpg','2015-07-07 13:52:54','2015-07-07 14:52:54',0,'Bubbles girl 3',10515,150,58,0,2,150,150,'image/jpeg',0),
	(3,'content','default','bubbles-3.jpg','orig',0,'jpg','2015-07-07 13:53:04','2015-07-07 14:53:04',0,'Bubbles 3',42220,800,470,0,1,NULL,NULL,'image/jpeg',0),
	(4,'content','default','bubbles-3-thumb@2x.jpg','thumb',3,'jpg','2015-07-07 13:53:04','2015-07-07 14:53:04',0,'Bubbles 3',9144,150,88,0,2,150,150,'image/jpeg',0),
	(5,'content','default','bubbles-girl-3-w200.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w200',5648,200,78,0,1,200,0,'image/jpeg',0),
	(6,'content','default','bubbles-girl-3-w400.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w400',16023,400,157,0,1,400,0,'image/jpeg',0),
	(7,'content','default','bubbles-girl-3-w800.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w800',48158,800,314,0,1,800,0,'image/jpeg',0),
	(8,'content','default','bubbles-girl-3-w1200.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w1200',88792,1200,471,0,1,1200,0,'image/jpeg',0),
	(9,'content','default','bubbles-girl-3-w1600.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w1600',129821,1400,550,0,1,1600,0,'',0),
	(10,'content','default','bubbles-girl-3-w2000.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w2000',129821,1400,550,0,1,2000,0,'',0),
	(11,'content','default','bubbles-3-w200.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w200',5375,200,117,0,1,200,0,'image/jpeg',0),
	(12,'content','default','bubbles-3-w400.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w400',14133,400,235,0,1,400,0,'image/jpeg',0),
	(13,'content','default','bubbles-3-w800.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w800',42220,800,470,0,1,800,0,'',0),
	(14,'content','default','bubbles-3-w1200.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w1200',42220,800,470,0,1,1200,0,'',0),
	(15,'content','default','bubbles-3-w1600.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w1600',42220,800,470,0,1,1600,0,'',0),
	(16,'content','default','bubbles-3-w2000.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w2000',42220,800,470,0,1,2000,0,'',0),
	(17,'content','default','services.png','orig',0,'png','2015-07-07 15:03:44','2015-07-07 16:03:44',0,'Services',1687877,2234,1072,0,1,NULL,NULL,'image/png',0),
	(18,'content','default','services-thumb@2x.png','thumb',17,'png','2015-07-07 15:03:44','2015-07-07 16:03:44',0,'Services',58154,150,71,0,2,150,150,'image/png',0),
	(19,'content','default','services-w200.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w200',27994,200,95,0,1,200,0,'image/png',0),
	(20,'content','default','services-w400.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w400',97775,400,191,0,1,400,0,'image/png',0),
	(21,'content','default','services-w800.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w800',349747,800,383,0,1,800,0,'image/png',0),
	(22,'content','default','services-w1200.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w1200',738819,1200,575,0,1,1200,0,'image/png',0),
	(23,'content','default','services-w1600.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w1600',1239582,1600,767,0,1,1600,0,'image/png',0),
	(24,'content','default','services-w2000.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w2000',1821700,2000,959,0,1,2000,0,'image/png',0),
	(25,'content','default','happy-clients.png','orig',0,'png','2015-07-07 15:09:05','2015-07-07 16:09:05',0,'Happy clients',1855777,2234,1072,0,1,NULL,NULL,'image/png',0),
	(26,'content','default','happy-clients-thumb@2x.png','thumb',25,'png','2015-07-07 15:09:05','2015-07-07 16:09:05',0,'Happy clients',66056,150,71,0,2,150,150,'image/png',0),
	(27,'content','default','happy-clients-w200.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w200',33674,200,95,0,1,200,0,'image/png',0),
	(28,'content','default','happy-clients-w400.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w400',107139,400,191,0,1,400,0,'image/png',0),
	(29,'content','default','happy-clients-w800.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w800',359700,800,383,0,1,800,0,'image/png',0),
	(30,'content','default','happy-clients-w1200.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w1200',760323,1200,575,0,1,1200,0,'image/png',0),
	(31,'content','default','happy-clients-w1600.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w1600',1292617,1600,767,0,1,1600,0,'image/png',0),
	(32,'content','default','happy-clients-w2000.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w2000',1914935,2000,959,0,1,2000,0,'image/png',0),
	(33,'content','default','contact.png','orig',0,'png','2015-07-07 15:15:20','2015-07-07 16:15:20',0,'Contact',1969945,2234,1072,0,1,NULL,NULL,'image/png',0),
	(34,'content','default','contact-thumb@2x.png','thumb',33,'png','2015-07-07 15:15:20','2015-07-07 16:15:20',0,'Contact',71297,150,71,0,2,150,150,'image/png',0),
	(35,'content','default','contact-w200.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w200',34959,200,95,0,1,200,0,'image/png',0),
	(36,'content','default','contact-w400.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w400',117732,400,191,0,1,400,0,'image/png',0),
	(37,'content','default','contact-w800.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w800',390771,800,383,0,1,800,0,'image/png',0),
	(38,'content','default','contact-w1200.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w1200',800902,1200,575,0,1,1200,0,'image/png',0),
	(39,'content','default','contact-w1600.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w1600',1338008,1600,767,0,1,1600,0,'image/png',0),
	(40,'content','default','contact-w2000.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w2000',1978480,2000,959,0,1,2000,0,'image/png',0);

/*!40000 ALTER TABLE `perch2_resources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_resources_to_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_resources_to_tags`;

CREATE TABLE `perch2_resources_to_tags` (
  `resourceID` int(10) NOT NULL DEFAULT '0',
  `tagID` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`resourceID`,`tagID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;



# Dump of table perch2_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_settings`;

CREATE TABLE `perch2_settings` (
  `settingID` varchar(60) NOT NULL DEFAULT '',
  `userID` int(10) unsigned NOT NULL DEFAULT '0',
  `settingValue` text NOT NULL,
  PRIMARY KEY (`settingID`,`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_settings` WRITE;
/*!40000 ALTER TABLE `perch2_settings` DISABLE KEYS */;

INSERT INTO `perch2_settings` (`settingID`, `userID`, `settingValue`)
VALUES
	('headerColour',0,'rgb(54,54,54)'),
	('content_singlePageEdit',0,'1'),
	('helpURL',0,''),
	('siteURL',0,'/'),
	('hideBranding',0,'0'),
	('content_collapseList',0,'1'),
	('lang',0,'en-gb'),
	('update_2.8.8',0,'done'),
	('headerScheme',0,'dark'),
	('update_runway_2.8.8',0,'done'),
	('latest_version',0,'2.8.8'),
	('on_sale_version',0,'2.8.8');

/*!40000 ALTER TABLE `perch2_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_user_privileges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_user_privileges`;

CREATE TABLE `perch2_user_privileges` (
  `privID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `privKey` varchar(255) NOT NULL DEFAULT '',
  `privTitle` varchar(255) NOT NULL DEFAULT '',
  `privOrder` int(10) unsigned NOT NULL DEFAULT '99',
  PRIMARY KEY (`privID`),
  UNIQUE KEY `idx_key` (`privKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_user_privileges` WRITE;
/*!40000 ALTER TABLE `perch2_user_privileges` DISABLE KEYS */;

INSERT INTO `perch2_user_privileges` (`privID`, `privKey`, `privTitle`, `privOrder`)
VALUES
	(1,'perch.login','Log in',1),
	(2,'perch.settings','Change settings',2),
	(3,'perch.users.manage','Manage users',3),
	(4,'perch.updatenotices','View update notices',4),
	(5,'content.regions.delete','Delete regions',1),
	(6,'content.regions.options','Edit region options',2),
	(7,'content.pages.edit','Edit page details',1),
	(8,'content.pages.reorder','Reorder pages',2),
	(9,'content.pages.create','Add new pages',3),
	(10,'content.pages.configure','Configure page settings',5),
	(11,'content.pages.delete','Delete pages',4),
	(12,'content.templates.delete','Delete master pages',6),
	(13,'content.navgroups.configure','Configure navigation groups',7),
	(14,'content.navgroups.create','Create navigation groups',8),
	(15,'content.navgroups.delete','Delete navigation groups',9),
	(16,'content.pages.create.toplevel','Add new top-level pages',3),
	(17,'content.pages.delete.own','Delete pages they created themselves',4),
	(18,'content.templates.configure','Configure master pages',6),
	(19,'content.pages.attributes','Edit page titles and attributes',6),
	(20,'assets.create','Upload assets',2),
	(21,'content.pages.republish','Republish pages',12),
	(22,'assets.manage','Manage assets',2),
	(23,'categories.create','Create new categories',1),
	(24,'categories.delete','Delete categories',2),
	(25,'categories.manage','Manage categories',3),
	(26,'categories.sets.create','Create category sets',4),
	(27,'categories.sets.delete','Delete category sets',5),
	(28,'content.regions.revert','Roll back regions',3);

/*!40000 ALTER TABLE `perch2_user_privileges` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_user_role_privileges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_user_role_privileges`;

CREATE TABLE `perch2_user_role_privileges` (
  `roleID` int(10) unsigned NOT NULL,
  `privID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`roleID`,`privID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_user_role_privileges` WRITE;
/*!40000 ALTER TABLE `perch2_user_role_privileges` DISABLE KEYS */;

INSERT INTO `perch2_user_role_privileges` (`roleID`, `privID`)
VALUES
	(1,1),
	(1,7),
	(1,8),
	(1,25),
	(2,1),
	(2,2),
	(2,3),
	(2,4),
	(2,5),
	(2,6),
	(2,7),
	(2,8),
	(2,9),
	(2,10),
	(2,11),
	(2,12);

/*!40000 ALTER TABLE `perch2_user_role_privileges` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_user_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_user_roles`;

CREATE TABLE `perch2_user_roles` (
  `roleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roleTitle` varchar(255) NOT NULL DEFAULT '',
  `roleSlug` varchar(255) NOT NULL DEFAULT '',
  `roleMasterAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`roleID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_user_roles` WRITE;
/*!40000 ALTER TABLE `perch2_user_roles` DISABLE KEYS */;

INSERT INTO `perch2_user_roles` (`roleID`, `roleTitle`, `roleSlug`, `roleMasterAdmin`)
VALUES
	(1,'Editor','editor',0),
	(2,'Admin','admin',1);

/*!40000 ALTER TABLE `perch2_user_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_users`;

CREATE TABLE `perch2_users` (
  `userID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userUsername` varchar(255) NOT NULL DEFAULT '',
  `userPassword` varchar(255) NOT NULL DEFAULT '',
  `userCreated` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `userUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userLastLogin` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `userGivenName` varchar(255) NOT NULL DEFAULT '',
  `userFamilyName` varchar(255) NOT NULL DEFAULT '',
  `userEmail` varchar(255) NOT NULL DEFAULT '',
  `userEnabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userHash` char(32) NOT NULL DEFAULT '',
  `roleID` int(10) unsigned NOT NULL DEFAULT '1',
  `userMasterAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userID`),
  KEY `idx_enabled` (`userEnabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_users` WRITE;
/*!40000 ALTER TABLE `perch2_users` DISABLE KEYS */;

INSERT INTO `perch2_users` (`userID`, `userUsername`, `userPassword`, `userCreated`, `userUpdated`, `userLastLogin`, `userGivenName`, `userFamilyName`, `userEmail`, `userEnabled`, `userHash`, `roleID`, `userMasterAdmin`)
VALUES
	(1,'s.meehan','$P$BILhLIYvywit6Prc2MV96KkmQF3L371','2015-07-03 12:35:42','2015-07-08 17:30:53','2015-07-08 14:56:34','Stephen','Meehan','s.meehan@d3creative.uk',1,'13b714a80eb5adf61e2b998df5c3679a',2,1);

/*!40000 ALTER TABLE `perch2_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
