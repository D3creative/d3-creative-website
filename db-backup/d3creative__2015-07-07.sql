# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.42)
# Database: d3creative_
# Generation Time: 2015-07-07 16:44:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table perch2_backup_plans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_backup_plans`;

CREATE TABLE `perch2_backup_plans` (
  `planID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `planTitle` char(255) NOT NULL DEFAULT '',
  `planRole` enum('all','db') NOT NULL DEFAULT 'all',
  `planCreated` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `planCreatedBy` char(32) NOT NULL DEFAULT '',
  `planUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `planUpdatedBy` char(32) NOT NULL DEFAULT '',
  `planActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `planDynamicFields` text NOT NULL,
  `planFrequency` int(10) unsigned NOT NULL DEFAULT '24',
  `planBucket` char(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`planID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_backup_resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_backup_resources`;

CREATE TABLE `perch2_backup_resources` (
  `planID` int(10) unsigned NOT NULL,
  `resourceID` int(10) unsigned NOT NULL,
  `runID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`planID`,`resourceID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_backup_runs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_backup_runs`;

CREATE TABLE `perch2_backup_runs` (
  `runID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `planID` int(10) unsigned NOT NULL,
  `runDateTime` datetime NOT NULL,
  `runType` enum('db','resources') NOT NULL DEFAULT 'resources',
  `runResult` enum('OK','FAILED','IN PROGRESS') NOT NULL DEFAULT 'OK',
  `runMessage` char(255) NOT NULL DEFAULT '',
  `runDbFile` char(255) NOT NULL,
  PRIMARY KEY (`runID`),
  KEY `idx_plan` (`planID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_categories`;

CREATE TABLE `perch2_categories` (
  `catID` int(10) NOT NULL AUTO_INCREMENT,
  `setID` int(10) unsigned NOT NULL,
  `catParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `catTitle` char(64) NOT NULL DEFAULT '',
  `catSlug` char(64) NOT NULL DEFAULT '',
  `catPath` char(255) NOT NULL DEFAULT '',
  `catDisplayPath` char(255) NOT NULL DEFAULT '',
  `catOrder` int(10) unsigned NOT NULL DEFAULT '0',
  `catTreePosition` char(255) NOT NULL DEFAULT '000',
  `catDynamicFields` text NOT NULL,
  PRIMARY KEY (`catID`),
  KEY `idx_set` (`setID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_category_counts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_category_counts`;

CREATE TABLE `perch2_category_counts` (
  `countID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catID` int(10) unsigned NOT NULL,
  `countType` char(64) NOT NULL DEFAULT '',
  `countValue` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`countID`),
  KEY `idx_cat` (`catID`),
  KEY `idx_cat_type` (`countType`,`catID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_category_sets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_category_sets`;

CREATE TABLE `perch2_category_sets` (
  `setID` int(10) NOT NULL AUTO_INCREMENT,
  `setTitle` char(64) NOT NULL DEFAULT '',
  `setSlug` char(64) NOT NULL DEFAULT '',
  `setTemplate` char(255) NOT NULL DEFAULT 'set.html',
  `setCatTemplate` char(255) NOT NULL DEFAULT 'category.html',
  `setDynamicFields` text,
  PRIMARY KEY (`setID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_collection_index
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_collection_index`;

CREATE TABLE `perch2_collection_index` (
  `indexID` int(10) NOT NULL AUTO_INCREMENT,
  `itemID` int(10) NOT NULL DEFAULT '0',
  `collectionID` int(10) NOT NULL DEFAULT '0',
  `itemRev` int(10) NOT NULL DEFAULT '0',
  `indexKey` char(64) NOT NULL DEFAULT '-',
  `indexValue` char(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`indexID`),
  KEY `idx_key` (`indexKey`),
  KEY `idx_val` (`indexValue`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_item` (`itemID`),
  KEY `idx_keyval` (`indexKey`,`indexValue`),
  KEY `idx_colrev` (`collectionID`,`itemRev`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_collection_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_collection_items`;

CREATE TABLE `perch2_collection_items` (
  `itemRowID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(10) unsigned NOT NULL,
  `itemRev` int(10) unsigned NOT NULL DEFAULT '0',
  `collectionID` int(10) unsigned NOT NULL,
  `itemJSON` mediumtext NOT NULL,
  `itemSearch` mediumtext NOT NULL,
  `itemUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `itemUpdatedBy` char(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemRowID`),
  KEY `idx_item` (`itemID`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_collection` (`collectionID`),
  KEY `idx_regrev` (`itemID`,`collectionID`,`itemRev`),
  FULLTEXT KEY `idx_search` (`itemSearch`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_collection_revisions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_collection_revisions`;

CREATE TABLE `perch2_collection_revisions` (
  `itemID` int(10) unsigned NOT NULL,
  `collectionID` int(10) unsigned NOT NULL,
  `itemOrder` int(10) unsigned DEFAULT '1000',
  `itemRev` int(10) unsigned NOT NULL,
  `itemLatestRev` int(10) unsigned NOT NULL,
  `itemCreated` datetime NOT NULL DEFAULT '2014-02-21 06:53:00',
  `itemCreatedBy` char(32) NOT NULL DEFAULT '',
  `itemSearchable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`itemID`),
  KEY `idx_order` (`itemOrder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_collections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_collections`;

CREATE TABLE `perch2_collections` (
  `collectionID` int(10) NOT NULL AUTO_INCREMENT,
  `collectionKey` char(64) NOT NULL DEFAULT '',
  `collectionOrder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `collectionTemplate` char(255) NOT NULL DEFAULT '',
  `collectionOptions` text NOT NULL,
  `collectionSearchable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `collectionEditRoles` char(255) NOT NULL DEFAULT '*',
  `collectionUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `collectionInAppMenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`collectionID`),
  KEY `idx_key` (`collectionKey`),
  KEY `idx_appmenu` (`collectionInAppMenu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_content_index
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_content_index`;

CREATE TABLE `perch2_content_index` (
  `indexID` int(10) NOT NULL AUTO_INCREMENT,
  `itemID` int(10) NOT NULL DEFAULT '0',
  `regionID` int(10) NOT NULL DEFAULT '0',
  `pageID` int(10) NOT NULL DEFAULT '0',
  `itemRev` int(10) NOT NULL DEFAULT '0',
  `indexKey` char(64) NOT NULL DEFAULT '-',
  `indexValue` char(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`indexID`),
  KEY `idx_key` (`indexKey`),
  KEY `idx_val` (`indexValue`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_item` (`itemID`),
  KEY `idx_keyval` (`indexKey`,`indexValue`),
  KEY `idx_regrev` (`regionID`,`itemRev`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_content_index` WRITE;
/*!40000 ALTER TABLE `perch2_content_index` DISABLE KEYS */;

INSERT INTO `perch2_content_index` (`indexID`, `itemID`, `regionID`, `pageID`, `itemRev`, `indexKey`, `indexValue`)
VALUES
	(1,1,6,1,1,'text','Manchester-based website design and development services'),
	(2,1,6,1,1,'textarea','Mobile-friendly, content managed websites designed for business.'),
	(3,1,6,1,1,'align-text','left'),
	(4,1,6,1,1,'btn-txt-primary','Services'),
	(5,1,6,1,1,'btn-link-primary','/services'),
	(6,1,6,1,1,'btn-txt-secondary','Happy clients'),
	(7,1,6,1,1,'btn-link-secondary','/happy-clients'),
	(8,1,6,1,1,'image','/perch/resources/bubbles-girl-3.jpg'),
	(9,1,6,1,1,'imagecloseup','/perch/resources/bubbles-3.jpg'),
	(10,1,6,1,1,'alt','Girl blowing bubbles'),
	(11,1,6,1,1,'_id','1'),
	(12,1,6,1,1,'_order','1000'),
	(13,2,7,4,1,'text','Websites crafted for where your customers are - everywhere.'),
	(14,2,7,4,1,'textarea','A complete website solution'),
	(15,2,7,4,1,'align-text','left'),
	(16,2,7,4,1,'btn-txt-primary','Happy clients'),
	(17,2,7,4,1,'btn-link-primary','/happy-clients'),
	(18,2,7,4,1,'btn-txt-secondary','Contact us'),
	(19,2,7,4,1,'btn-link-secondary','/contact-us'),
	(20,2,7,4,1,'image','/perch/resources/services.png'),
	(21,2,7,4,1,'imagecloseup',''),
	(22,2,7,4,1,'alt','Creating a website'),
	(23,2,7,4,1,'_id','2'),
	(24,2,7,4,1,'_order','1000'),
	(25,3,11,5,1,'text','Making clients happy since 2005 we strive to be the best'),
	(26,3,11,5,1,'textarea','We design, develop and deliver'),
	(27,3,11,5,1,'align-text','left'),
	(28,3,11,5,1,'btn-txt-primary','Services'),
	(29,3,11,5,1,'btn-link-primary','/services'),
	(30,3,11,5,1,'btn-txt-secondary','Contact us'),
	(31,3,11,5,1,'btn-link-secondary','/contact'),
	(32,3,11,5,1,'image','/perch/resources/happy-clients.png'),
	(33,3,11,5,1,'imagecloseup',''),
	(34,3,11,5,1,'alt','Happy client'),
	(35,3,11,5,1,'_id','3'),
	(36,3,11,5,1,'_order','1000'),
	(37,4,13,6,1,'text','I welcome the opportunity to talk to you about your business'),
	(38,4,13,6,1,'textarea','Please complete all the fields below to get in touch'),
	(39,4,13,6,1,'align-text','left'),
	(40,4,13,6,1,'btn-txt-primary',''),
	(41,4,13,6,1,'btn-link-primary',''),
	(42,4,13,6,1,'btn-txt-secondary',''),
	(43,4,13,6,1,'btn-link-secondary',''),
	(44,4,13,6,1,'image','/perch/resources/contact.png'),
	(45,4,13,6,1,'imagecloseup',''),
	(46,4,13,6,1,'alt','Contact D3 Creative'),
	(47,4,13,6,1,'_id','4'),
	(48,4,13,6,1,'_order','1000');

/*!40000 ALTER TABLE `perch2_content_index` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_content_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_content_items`;

CREATE TABLE `perch2_content_items` (
  `itemRowID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(10) unsigned NOT NULL DEFAULT '0',
  `regionID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageID` int(10) unsigned NOT NULL DEFAULT '0',
  `itemRev` int(10) unsigned NOT NULL DEFAULT '0',
  `itemOrder` int(10) unsigned NOT NULL DEFAULT '1000',
  `itemJSON` mediumtext NOT NULL,
  `itemSearch` mediumtext NOT NULL,
  `itemUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `itemUpdatedBy` char(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemRowID`),
  KEY `idx_item` (`itemID`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_region` (`regionID`),
  KEY `idx_regrev` (`itemID`,`regionID`,`itemRev`),
  KEY `idx_order` (`itemOrder`),
  FULLTEXT KEY `idx_search` (`itemSearch`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_content_items` WRITE;
/*!40000 ALTER TABLE `perch2_content_items` DISABLE KEYS */;

INSERT INTO `perch2_content_items` (`itemRowID`, `itemID`, `regionID`, `pageID`, `itemRev`, `itemOrder`, `itemJSON`, `itemSearch`, `itemUpdated`, `itemUpdatedBy`)
VALUES
	(1,1,6,1,0,1000,'','','2015-07-07 14:51:23',''),
	(2,1,6,1,1,1000,'{\"_id\":\"1\",\"text\":\"Manchester-based website design and development services\",\"_title\":\"Manchester-based website design and development services \\/services \\/happy-clients Girl blowing bubbles\",\"textarea\":{\"raw\":\"Mobile-friendly, content managed websites designed for business.\",\"processed\":\"<p>Mobile-friendly, content managed websites designed for business.<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Services\",\"btn-link-primary\":\"\\/services\",\"btn-txt-secondary\":\"Happy clients\",\"btn-link-secondary\":\"\\/happy-clients\",\"image\":{\"assetID\":\"1\",\"title\":\"Bubbles girl 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-girl-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-girl-3.jpg\",\"size\":129821,\"w\":1400,\"h\":550,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"58\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-girl-3-thumb@2x.jpg\",\"size\":10515,\"mime\":\"\",\"assetID\":\"2\"},\"w200hc0\":{\"w\":200,\"h\":78,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w200.jpg\",\"size\":5648,\"mime\":\"image\\/jpeg\"},\"w400hc0\":{\"w\":400,\"h\":157,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w400.jpg\",\"size\":16023,\"mime\":\"image\\/jpeg\"},\"w800hc0\":{\"w\":800,\"h\":314,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w800.jpg\",\"size\":48158,\"mime\":\"image\\/jpeg\"},\"w1200hc0\":{\"w\":1200,\"h\":471,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1200.jpg\",\"size\":88792,\"mime\":\"image\\/jpeg\"},\"w1600hc0\":{\"w\":1400,\"h\":550,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w1600.jpg\",\"size\":129821,\"mime\":\"\"},\"w2000hc0\":{\"w\":1400,\"h\":550,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-girl-3-w2000.jpg\",\"size\":129821,\"mime\":\"\"}}},\"imagecloseup\":{\"assetID\":\"3\",\"title\":\"Bubbles 3\",\"_default\":\"\\/perch\\/resources\\/bubbles-3.jpg\",\"bucket\":\"default\",\"path\":\"bubbles-3.jpg\",\"size\":42220,\"w\":800,\"h\":470,\"mime\":\"image\\/jpeg\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"88\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"bubbles-3-thumb@2x.jpg\",\"size\":9144,\"mime\":\"\",\"assetID\":\"4\"},\"w200hc0\":{\"w\":200,\"h\":117,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w200.jpg\",\"size\":5375,\"mime\":\"image\\/jpeg\"},\"w400hc0\":{\"w\":400,\"h\":235,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w400.jpg\",\"size\":14133,\"mime\":\"image\\/jpeg\"},\"w800hc0\":{\"w\":800,\"h\":470,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w800.jpg\",\"size\":42220,\"mime\":\"\"},\"w1200hc0\":{\"w\":800,\"h\":470,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1200.jpg\",\"size\":42220,\"mime\":\"\"},\"w1600hc0\":{\"w\":800,\"h\":470,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w1600.jpg\",\"size\":42220,\"mime\":\"\"},\"w2000hc0\":{\"w\":800,\"h\":470,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"bubbles-3-w2000.jpg\",\"size\":42220,\"mime\":\"\"}}},\"alt\":\"Girl blowing bubbles\"}',' Manchester-based website design and development services Mobile-friendly, content managed websites designed for business. left Services /services Happy clients /happy-clients   Girl blowing bubbles ','2015-07-07 14:53:18','1'),
	(3,2,7,4,0,1000,'','','2015-07-07 16:02:43',''),
	(4,2,7,4,1,1000,'{\"_id\":\"2\",\"text\":\"Websites crafted for where your customers are - everywhere.\",\"_title\":\"Websites crafted for where your customers are - everywhere. \\/happy-clients \\/contact-us Creating a website\",\"textarea\":{\"raw\":\"A complete website solution\",\"processed\":\"<p>A complete website solution<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Happy clients\",\"btn-link-primary\":\"\\/happy-clients\",\"btn-txt-secondary\":\"Contact us\",\"btn-link-secondary\":\"\\/contact-us\",\"image\":{\"assetID\":\"17\",\"title\":\"Services\",\"_default\":\"\\/perch\\/resources\\/services.png\",\"bucket\":\"default\",\"path\":\"services.png\",\"size\":1687877,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"services-thumb@2x.png\",\"size\":58154,\"mime\":\"\",\"assetID\":\"18\"},\"w200hc0\":{\"w\":200,\"h\":95,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w200.png\",\"size\":27994,\"mime\":\"image\\/png\"},\"w400hc0\":{\"w\":400,\"h\":191,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w400.png\",\"size\":97775,\"mime\":\"image\\/png\"},\"w800hc0\":{\"w\":800,\"h\":383,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w800.png\",\"size\":349747,\"mime\":\"image\\/png\"},\"w1200hc0\":{\"w\":1200,\"h\":575,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w1200.png\",\"size\":738819,\"mime\":\"image\\/png\"},\"w1600hc0\":{\"w\":1600,\"h\":767,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w1600.png\",\"size\":1239582,\"mime\":\"image\\/png\"},\"w2000hc0\":{\"w\":2000,\"h\":959,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"services-w2000.png\",\"size\":1821700,\"mime\":\"image\\/png\"}}},\"imagecloseup\":null,\"alt\":\"Creating a website\"}',' Websites crafted for where your customers are - everywhere. A complete website solution left Happy clients /happy-clients Contact us /contact-us   Creating a website ','2015-07-07 16:04:31','1'),
	(5,3,11,5,0,1000,'','','2015-07-07 16:08:21',''),
	(6,3,11,5,1,1000,'{\"_id\":\"3\",\"text\":\"Making clients happy since 2005 we strive to be the best\",\"_title\":\"Making clients happy since 2005 we strive to be the best \\/services \\/contact Happy client\",\"textarea\":{\"raw\":\"We design, develop and deliver\",\"processed\":\"<p>We design, develop and deliver<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":\"Services\",\"btn-link-primary\":\"\\/services\",\"btn-txt-secondary\":\"Contact us\",\"btn-link-secondary\":\"\\/contact\",\"image\":{\"assetID\":\"25\",\"title\":\"Happy clients\",\"_default\":\"\\/perch\\/resources\\/happy-clients.png\",\"bucket\":\"default\",\"path\":\"happy-clients.png\",\"size\":1855777,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"happy-clients-thumb@2x.png\",\"size\":66056,\"mime\":\"\",\"assetID\":\"26\"},\"w200hc0\":{\"w\":200,\"h\":95,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w200.png\",\"size\":33674,\"mime\":\"image\\/png\"},\"w400hc0\":{\"w\":400,\"h\":191,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w400.png\",\"size\":107139,\"mime\":\"image\\/png\"},\"w800hc0\":{\"w\":800,\"h\":383,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w800.png\",\"size\":359700,\"mime\":\"image\\/png\"},\"w1200hc0\":{\"w\":1200,\"h\":575,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w1200.png\",\"size\":760323,\"mime\":\"image\\/png\"},\"w1600hc0\":{\"w\":1600,\"h\":767,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w1600.png\",\"size\":1292617,\"mime\":\"image\\/png\"},\"w2000hc0\":{\"w\":2000,\"h\":959,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"happy-clients-w2000.png\",\"size\":1914935,\"mime\":\"image\\/png\"}}},\"imagecloseup\":null,\"alt\":\"Happy client\"}',' Making clients happy since 2005 we strive to be the best We design, develop and deliver left Services /services Contact us /contact   Happy client ','2015-07-07 16:09:39','1'),
	(7,4,13,6,0,1000,'','','2015-07-07 16:15:04',''),
	(8,4,13,6,1,1000,'{\"_id\":\"4\",\"text\":\"I welcome the opportunity to talk to you about your business\",\"_title\":\"I welcome the opportunity to talk to you about your business Contact D3 Creative\",\"textarea\":{\"raw\":\"Please complete all the fields below to get in touch\",\"processed\":\"<p>Please complete all the fields below to get in touch<\\/p>\"},\"align-text\":\"left\",\"btn-txt-primary\":null,\"btn-link-primary\":null,\"btn-txt-secondary\":null,\"btn-link-secondary\":null,\"image\":{\"assetID\":\"33\",\"title\":\"Contact\",\"_default\":\"\\/perch\\/resources\\/contact.png\",\"bucket\":\"default\",\"path\":\"contact.png\",\"size\":1969945,\"w\":2234,\"h\":1072,\"mime\":\"image\\/png\",\"sizes\":{\"thumb\":{\"w\":\"150\",\"h\":\"71\",\"target_w\":150,\"target_h\":150,\"density\":2,\"path\":\"contact-thumb@2x.png\",\"size\":71297,\"mime\":\"\",\"assetID\":\"34\"},\"w200hc0\":{\"w\":200,\"h\":95,\"target_w\":\"200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w200.png\",\"size\":34959,\"mime\":\"image\\/png\"},\"w400hc0\":{\"w\":400,\"h\":191,\"target_w\":\"400\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w400.png\",\"size\":117732,\"mime\":\"image\\/png\"},\"w800hc0\":{\"w\":800,\"h\":383,\"target_w\":\"800\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w800.png\",\"size\":390771,\"mime\":\"image\\/png\"},\"w1200hc0\":{\"w\":1200,\"h\":575,\"target_w\":\"1200\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w1200.png\",\"size\":800902,\"mime\":\"image\\/png\"},\"w1600hc0\":{\"w\":1600,\"h\":767,\"target_w\":\"1600\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w1600.png\",\"size\":1338008,\"mime\":\"image\\/png\"},\"w2000hc0\":{\"w\":2000,\"h\":959,\"target_w\":\"2000\",\"target_h\":false,\"crop\":false,\"density\":\"1\",\"path\":\"contact-w2000.png\",\"size\":1978480,\"mime\":\"image\\/png\"}}},\"imagecloseup\":null,\"alt\":\"Contact D3 Creative\"}',' I welcome the opportunity to talk to you about your business Please complete all the fields below to get in touch left       Contact D3 Creative ','2015-07-07 16:15:59','1');

/*!40000 ALTER TABLE `perch2_content_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_content_regions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_content_regions`;

CREATE TABLE `perch2_content_regions` (
  `regionID` int(10) NOT NULL AUTO_INCREMENT,
  `pageID` int(10) unsigned NOT NULL,
  `regionKey` varchar(255) NOT NULL DEFAULT '',
  `regionPage` varchar(255) NOT NULL DEFAULT '',
  `regionHTML` longtext NOT NULL,
  `regionNew` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `regionOrder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `regionTemplate` varchar(255) NOT NULL DEFAULT '',
  `regionMultiple` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `regionOptions` text NOT NULL,
  `regionSearchable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `regionRev` int(10) unsigned NOT NULL DEFAULT '0',
  `regionLatestRev` int(10) unsigned NOT NULL DEFAULT '0',
  `regionEditRoles` varchar(255) NOT NULL DEFAULT '*',
  `regionUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`regionID`),
  KEY `idx_key` (`regionKey`),
  KEY `idx_path` (`regionPage`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_content_regions` WRITE;
/*!40000 ALTER TABLE `perch2_content_regions` DISABLE KEYS */;

INSERT INTO `perch2_content_regions` (`regionID`, `pageID`, `regionKey`, `regionPage`, `regionHTML`, `regionNew`, `regionOrder`, `regionTemplate`, `regionMultiple`, `regionOptions`, `regionSearchable`, `regionRev`, `regionLatestRev`, `regionEditRoles`, `regionUpdated`)
VALUES
	(16,1,'CTA Large','/','<!-- Undefined content: CTA Large -->',1,2,'',0,'',1,0,0,'*','2015-07-07 14:49:51'),
	(2,1,'Intro','/','<!-- Undefined content: Intro -->',1,1,'',0,'',1,0,0,'*','2015-07-03 13:37:14'),
	(3,3,'Error description','/errors/404','<!-- Undefined content: Error description -->',1,0,'',0,'',1,0,0,'*','2015-07-03 13:47:04'),
	(15,1,'Main','/','<!-- Undefined content: Main -->',1,1,'',0,'',1,0,0,'*','2015-07-07 14:49:51'),
	(11,5,'Hero','/happy-clients','<div class=\"row hero\"> \n    <div class=\"inner-container text-align left\">\n        <h1>Making clients happy since 2005 we strive to be the best</h1>\n        <p>We design, develop and deliver</p>\n       \n\n        <a class=\"btn primary\" title=\"Services\" href=\"/services\">Services</a><a class=\"btn secondary\" title=\"Contact us\" href=\"/contact\">Contact us</a>\n\n  </div>\n    <div class=\"wrap\">\n      \n       <picture>\n    <!--[if IE 9]><video style=\"display: none;\"><![endif]-->\n    <source\n    media=\"(min-width: 800px)\"\n    sizes=\"100vw\"\n    srcset=\"/perch/resources/happy-clients-w200.png 200w,   \n            /perch/resources/happy-clients-w400.png 400w,\n            /perch/resources/happy-clients-w800.png 800w,\n            /perch/resources/happy-clients-w1200.png 1200w,\n            /perch/resources/happy-clients-w1600.png 1600w,\n            /perch/resources/happy-clients-w2000.png 2000w\"\n    >\n\n  <!--[if IE 9]></video><![endif]-->\n    <img\n    src=\"/perch/resources/happy-clients-w2000.png\" alt=\"Happy client\">\n\n</picture>\n\n\n\n\n\n\n\n\n\n    </div>\n</div>\n\n\n\n\n\n\n\n',0,0,'blocks/hero-jumbo.html',0,'{\"edit_mode\":\"singlepage\"}',1,1,1,'*','2015-07-07 16:09:39'),
	(6,1,'Hero','/','<div class=\"row hero\"> \n    <div class=\"inner-container text-align left\">\n        <h1>Manchester-based website design and development services</h1>\n        <p>Mobile-friendly, content managed websites designed for business.</p>\n       \n\n        <a class=\"btn primary\" title=\"Services\" href=\"/services\">Services</a><a class=\"btn secondary\" title=\"Happy clients\" href=\"/happy-clients\">Happy clients</a>\n\n  </div>\n    <div class=\"wrap\">\n      \n       <picture>\n    <!--[if IE 9]><video style=\"display: none;\"><![endif]-->\n    <source\n    media=\"(min-width: 800px)\"\n    sizes=\"100vw\"\n    srcset=\"/perch/resources/bubbles-girl-3-w200.jpg 200w,   \n            /perch/resources/bubbles-girl-3-w400.jpg 400w,\n            /perch/resources/bubbles-girl-3-w800.jpg 800w,\n            /perch/resources/bubbles-girl-3-w1200.jpg 1200w,\n            /perch/resources/bubbles-girl-3-w1600.jpg 1600w,\n            /perch/resources/bubbles-girl-3-w2000.jpg 2000w\"\n    >\n\n  \n    <source\n    media=\"(max-width: 799px)\"\n    sizes=\"100vw\"\n    srcset=\"/perch/resources/bubbles-3-w200.jpg 200w,\n            /perch/resources/bubbles-3-w400.jpg 400w,\n            /perch/resources/bubbles-3-w800.jpg 800w,\n            /perch/resources/bubbles-3-w1200.jpg 1200w,\n            /perch/resources/bubbles-3-w1600.jpg 1600w,\n            /perch/resources/bubbles-3-w2000.jpg 2000w\"\n    ><!--[if IE 9]></video><![endif]-->\n    <img\n    src=\"/perch/resources/bubbles-girl-3-w2000.jpg\" alt=\"Girl blowing bubbles\">\n\n</picture>\n\n\n\n\n\n\n\n\n\n    </div>\n</div>\n\n\n\n\n\n\n\n',0,0,'blocks/hero-jumbo.html',0,'{\"edit_mode\":\"singlepage\"}',1,1,1,'*','2015-07-07 14:53:18'),
	(7,4,'Hero','/services','<div class=\"row hero\"> \n    <div class=\"inner-container text-align left\">\n        <h1>Websites crafted for where your customers are - everywhere.</h1>\n        <p>A complete website solution</p>\n       \n\n        <a class=\"btn primary\" title=\"Happy clients\" href=\"/happy-clients\">Happy clients</a><a class=\"btn secondary\" title=\"Contact us\" href=\"/contact-us\">Contact us</a>\n\n  </div>\n    <div class=\"wrap\">\n      \n       <picture>\n    <!--[if IE 9]><video style=\"display: none;\"><![endif]-->\n    <source\n    media=\"(min-width: 800px)\"\n    sizes=\"100vw\"\n    srcset=\"/perch/resources/services-w200.png 200w,   \n            /perch/resources/services-w400.png 400w,\n            /perch/resources/services-w800.png 800w,\n            /perch/resources/services-w1200.png 1200w,\n            /perch/resources/services-w1600.png 1600w,\n            /perch/resources/services-w2000.png 2000w\"\n    >\n\n  <!--[if IE 9]></video><![endif]-->\n    <img\n    src=\"/perch/resources/services-w2000.png\" alt=\"Creating a website\">\n\n</picture>\n\n\n\n\n\n\n\n\n\n    </div>\n</div>\n\n\n\n\n\n\n\n',0,0,'blocks/hero-jumbo.html',0,'{\"edit_mode\":\"singlepage\"}',1,1,1,'*','2015-07-07 16:04:31'),
	(9,1,'Latest project','/','<!-- Undefined content: Latest project -->',1,2,'',0,'',1,0,0,'*','2015-07-03 13:56:58'),
	(10,4,'Main','/services','<!-- Undefined content: Main -->',1,1,'',0,'',1,0,0,'*','2015-07-03 13:57:02'),
	(12,5,'Main','/happy-clients','<!-- Undefined content: Main -->',1,1,'',0,'',1,0,0,'*','2015-07-03 13:57:40'),
	(13,6,'Hero','/contact','<div class=\"row hero\"> \n    <div class=\"inner-container text-align left\">\n        <h1>I welcome the opportunity to talk to you about your business</h1>\n        <p>Please complete all the fields below to get in touch</p>\n       \n\n        \n\n  </div>\n    <div class=\"wrap\">\n      \n       <picture>\n    <!--[if IE 9]><video style=\"display: none;\"><![endif]-->\n    <source\n    media=\"(min-width: 800px)\"\n    sizes=\"100vw\"\n    srcset=\"/perch/resources/contact-w200.png 200w,   \n            /perch/resources/contact-w400.png 400w,\n            /perch/resources/contact-w800.png 800w,\n            /perch/resources/contact-w1200.png 1200w,\n            /perch/resources/contact-w1600.png 1600w,\n            /perch/resources/contact-w2000.png 2000w\"\n    >\n\n  <!--[if IE 9]></video><![endif]-->\n    <img\n    src=\"/perch/resources/contact-w2000.png\" alt=\"Contact D3 Creative\">\n\n</picture>\n\n\n\n\n\n\n\n\n\n    </div>\n</div>\n\n\n\n\n\n\n\n',0,0,'blocks/hero-jumbo.html',0,'{\"edit_mode\":\"singlepage\"}',1,1,1,'*','2015-07-07 16:15:59'),
	(14,6,'Main','/contact','<!-- Undefined content: Main -->',1,1,'',0,'',1,0,0,'*','2015-07-03 13:57:54'),
	(17,5,'CTA Large','/happy-clients','<!-- Undefined content: CTA Large -->',1,2,'',0,'',1,0,0,'*','2015-07-07 15:09:42'),
	(18,6,'CTA Large','/contact','<!-- Undefined content: CTA Large -->',1,2,'',0,'',1,0,0,'*','2015-07-07 15:10:35'),
	(19,4,'CTA Large','/services','<!-- Undefined content: CTA Large -->',1,2,'',0,'',1,0,0,'*','2015-07-07 15:13:12');

/*!40000 ALTER TABLE `perch2_content_regions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_navigation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_navigation`;

CREATE TABLE `perch2_navigation` (
  `groupID` int(10) NOT NULL AUTO_INCREMENT,
  `groupTitle` varchar(255) NOT NULL DEFAULT '',
  `groupSlug` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_navigation_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_navigation_pages`;

CREATE TABLE `perch2_navigation_pages` (
  `navpageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageID` int(10) unsigned NOT NULL DEFAULT '0',
  `groupID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageOrder` int(10) unsigned NOT NULL DEFAULT '1',
  `pageDepth` tinyint(10) unsigned NOT NULL,
  `pageTreePosition` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`navpageID`),
  KEY `idx_group` (`groupID`),
  KEY `idx_page_group` (`pageID`,`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_page_routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_page_routes`;

CREATE TABLE `perch2_page_routes` (
  `routeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageID` int(10) unsigned NOT NULL DEFAULT '0',
  `routePattern` char(255) NOT NULL DEFAULT '',
  `routeRegExp` char(255) NOT NULL DEFAULT '',
  `routeOrder` int(10) unsigned NOT NULL,
  PRIMARY KEY (`routeID`),
  KEY `idx_page` (`pageID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table perch2_page_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_page_templates`;

CREATE TABLE `perch2_page_templates` (
  `templateID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `templateTitle` varchar(255) NOT NULL DEFAULT '',
  `templatePath` varchar(255) NOT NULL DEFAULT '',
  `optionsPageID` int(10) unsigned NOT NULL DEFAULT '0',
  `templateReference` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `templateNavGroups` varchar(255) DEFAULT '',
  PRIMARY KEY (`templateID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_page_templates` WRITE;
/*!40000 ALTER TABLE `perch2_page_templates` DISABLE KEYS */;

INSERT INTO `perch2_page_templates` (`templateID`, `templateTitle`, `templatePath`, `optionsPageID`, `templateReference`, `templateNavGroups`)
VALUES
	(1,'Default','default.php',0,1,''),
	(2,'Home','home.php',0,1,''),
	(3,'Search','search.php',0,1,''),
	(4,'404','errors/404.php',0,1,''),
	(8,'Two - Col','two-col.php',0,1,'');

/*!40000 ALTER TABLE `perch2_page_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_pages`;

CREATE TABLE `perch2_pages` (
  `pageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `pagePath` varchar(255) NOT NULL DEFAULT '',
  `pageTitle` varchar(255) NOT NULL DEFAULT '',
  `pageNavText` varchar(255) NOT NULL DEFAULT '',
  `pageNew` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `pageOrder` int(10) unsigned NOT NULL DEFAULT '1',
  `pageDepth` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `pageSortPath` varchar(255) NOT NULL DEFAULT '',
  `pageTreePosition` varchar(64) NOT NULL DEFAULT '',
  `pageSubpageRoles` varchar(255) NOT NULL DEFAULT '',
  `pageSubpagePath` varchar(255) NOT NULL DEFAULT '',
  `pageHidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pageNavOnly` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pageAccessTags` varchar(255) NOT NULL DEFAULT '',
  `pageCreatorID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageModified` datetime NOT NULL DEFAULT '2014-01-01 00:00:00',
  `pageAttributes` text NOT NULL,
  `pageAttributeTemplate` varchar(255) NOT NULL DEFAULT 'default.html',
  `pageTemplate` char(255) NOT NULL DEFAULT '',
  `templateID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageSubpageTemplates` varchar(255) NOT NULL DEFAULT '',
  `pageCollections` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`pageID`),
  KEY `idx_parent` (`pageParentID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_pages` WRITE;
/*!40000 ALTER TABLE `perch2_pages` DISABLE KEYS */;

INSERT INTO `perch2_pages` (`pageID`, `pageParentID`, `pagePath`, `pageTitle`, `pageNavText`, `pageNew`, `pageOrder`, `pageDepth`, `pageSortPath`, `pageTreePosition`, `pageSubpageRoles`, `pageSubpagePath`, `pageHidden`, `pageNavOnly`, `pageAccessTags`, `pageCreatorID`, `pageModified`, `pageAttributes`, `pageAttributeTemplate`, `pageTemplate`, `templateID`, `pageSubpageTemplates`, `pageCollections`)
VALUES
	(1,0,'/','Home','Home',0,1,1,'/','000-001','','',0,0,'',1,'2015-07-07 13:53:18','{\"description\":{\"raw\":\"\",\"processed\":\"\"},\"keywords\":{\"raw\":\"\",\"processed\":\"\"},\"noindex\":null,\"nofollow\":null,\"nosnippet\":null}','default.html','home.php',2,'',''),
	(2,0,'/errors','Errors','Errors',0,2,1,'/errors','000-002','','',1,0,'',1,'2015-07-03 12:37:14','','default.html','errors/404.php',4,'',''),
	(3,2,'/errors/404','404','404',0,1,2,'/errors/404','000-002-001','','',1,0,'',1,'2015-07-03 12:37:14','','default.html','errors/404.php',4,'',''),
	(4,0,'/services','Services','Services',0,3,1,'/services','000-004','','',0,0,'',1,'2015-07-07 15:04:31','','default.html','default.php',1,'',''),
	(5,0,'/happy-clients','Happy clients','Happy clients',0,4,1,'/happy-clients','000-005','','',0,0,'',1,'2015-07-07 15:09:39','','default.html','default.php',1,'',''),
	(6,0,'/contact','Contact','Contact',0,5,1,'/contact','000-006','','',0,0,'',1,'2015-07-07 15:15:59','','default.html','default.php',1,'','');

/*!40000 ALTER TABLE `perch2_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_resource_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_resource_tags`;

CREATE TABLE `perch2_resource_tags` (
  `tagID` int(10) NOT NULL AUTO_INCREMENT,
  `tagTitle` varchar(255) NOT NULL DEFAULT '',
  `tagSlug` varchar(255) NOT NULL DEFAULT '',
  `tagCount` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tagID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



# Dump of table perch2_resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_resources`;

CREATE TABLE `perch2_resources` (
  `resourceID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resourceApp` char(32) NOT NULL DEFAULT 'content',
  `resourceBucket` char(16) NOT NULL DEFAULT 'default',
  `resourceFile` char(255) NOT NULL DEFAULT '',
  `resourceKey` enum('orig','thumb') DEFAULT NULL,
  `resourceParentID` int(10) NOT NULL DEFAULT '0',
  `resourceType` char(4) NOT NULL DEFAULT '',
  `resourceCreated` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `resourceUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `resourceAWOL` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `resourceTitle` char(255) DEFAULT NULL,
  `resourceFileSize` int(10) unsigned DEFAULT NULL,
  `resourceWidth` int(10) unsigned DEFAULT NULL,
  `resourceHeight` int(10) unsigned DEFAULT NULL,
  `resourceCrop` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `resourceDensity` float NOT NULL DEFAULT '1',
  `resourceTargetWidth` int(10) unsigned DEFAULT NULL,
  `resourceTargetHeight` int(10) unsigned DEFAULT NULL,
  `resourceMimeType` char(64) DEFAULT NULL,
  `resourceInLibrary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`resourceID`),
  UNIQUE KEY `idx_file` (`resourceBucket`,`resourceFile`),
  KEY `idx_app` (`resourceApp`),
  KEY `idx_key` (`resourceKey`),
  KEY `idx_type` (`resourceType`),
  KEY `idx_awol` (`resourceAWOL`),
  KEY `idx_library` (`resourceInLibrary`),
  FULLTEXT KEY `idx_search` (`resourceTitle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_resources` WRITE;
/*!40000 ALTER TABLE `perch2_resources` DISABLE KEYS */;

INSERT INTO `perch2_resources` (`resourceID`, `resourceApp`, `resourceBucket`, `resourceFile`, `resourceKey`, `resourceParentID`, `resourceType`, `resourceCreated`, `resourceUpdated`, `resourceAWOL`, `resourceTitle`, `resourceFileSize`, `resourceWidth`, `resourceHeight`, `resourceCrop`, `resourceDensity`, `resourceTargetWidth`, `resourceTargetHeight`, `resourceMimeType`, `resourceInLibrary`)
VALUES
	(1,'content','default','bubbles-girl-3.jpg','orig',0,'jpg','2015-07-07 13:52:54','2015-07-07 14:52:54',0,'Bubbles girl 3',129821,1400,550,0,1,NULL,NULL,'image/jpeg',0),
	(2,'content','default','bubbles-girl-3-thumb@2x.jpg','thumb',1,'jpg','2015-07-07 13:52:54','2015-07-07 14:52:54',0,'Bubbles girl 3',10515,150,58,0,2,150,150,'image/jpeg',0),
	(3,'content','default','bubbles-3.jpg','orig',0,'jpg','2015-07-07 13:53:04','2015-07-07 14:53:04',0,'Bubbles 3',42220,800,470,0,1,NULL,NULL,'image/jpeg',0),
	(4,'content','default','bubbles-3-thumb@2x.jpg','thumb',3,'jpg','2015-07-07 13:53:04','2015-07-07 14:53:04',0,'Bubbles 3',9144,150,88,0,2,150,150,'image/jpeg',0),
	(5,'content','default','bubbles-girl-3-w200.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w200',5648,200,78,0,1,200,0,'image/jpeg',0),
	(6,'content','default','bubbles-girl-3-w400.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w400',16023,400,157,0,1,400,0,'image/jpeg',0),
	(7,'content','default','bubbles-girl-3-w800.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w800',48158,800,314,0,1,800,0,'image/jpeg',0),
	(8,'content','default','bubbles-girl-3-w1200.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w1200',88792,1200,471,0,1,1200,0,'image/jpeg',0),
	(9,'content','default','bubbles-girl-3-w1600.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w1600',129821,1400,550,0,1,1600,0,'',0),
	(10,'content','default','bubbles-girl-3-w2000.jpg','',1,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles girl 3 w2000',129821,1400,550,0,1,2000,0,'',0),
	(11,'content','default','bubbles-3-w200.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w200',5375,200,117,0,1,200,0,'image/jpeg',0),
	(12,'content','default','bubbles-3-w400.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w400',14133,400,235,0,1,400,0,'image/jpeg',0),
	(13,'content','default','bubbles-3-w800.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w800',42220,800,470,0,1,800,0,'',0),
	(14,'content','default','bubbles-3-w1200.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w1200',42220,800,470,0,1,1200,0,'',0),
	(15,'content','default','bubbles-3-w1600.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w1600',42220,800,470,0,1,1600,0,'',0),
	(16,'content','default','bubbles-3-w2000.jpg','',3,'jpg','2015-07-07 13:53:18','2015-07-07 14:53:18',0,'Bubbles 3 w2000',42220,800,470,0,1,2000,0,'',0),
	(17,'content','default','services.png','orig',0,'png','2015-07-07 15:03:44','2015-07-07 16:03:44',0,'Services',1687877,2234,1072,0,1,NULL,NULL,'image/png',0),
	(18,'content','default','services-thumb@2x.png','thumb',17,'png','2015-07-07 15:03:44','2015-07-07 16:03:44',0,'Services',58154,150,71,0,2,150,150,'image/png',0),
	(19,'content','default','services-w200.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w200',27994,200,95,0,1,200,0,'image/png',0),
	(20,'content','default','services-w400.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w400',97775,400,191,0,1,400,0,'image/png',0),
	(21,'content','default','services-w800.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w800',349747,800,383,0,1,800,0,'image/png',0),
	(22,'content','default','services-w1200.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w1200',738819,1200,575,0,1,1200,0,'image/png',0),
	(23,'content','default','services-w1600.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w1600',1239582,1600,767,0,1,1600,0,'image/png',0),
	(24,'content','default','services-w2000.png','',17,'png','2015-07-07 15:04:31','2015-07-07 16:04:31',0,'Services w2000',1821700,2000,959,0,1,2000,0,'image/png',0),
	(25,'content','default','happy-clients.png','orig',0,'png','2015-07-07 15:09:05','2015-07-07 16:09:05',0,'Happy clients',1855777,2234,1072,0,1,NULL,NULL,'image/png',0),
	(26,'content','default','happy-clients-thumb@2x.png','thumb',25,'png','2015-07-07 15:09:05','2015-07-07 16:09:05',0,'Happy clients',66056,150,71,0,2,150,150,'image/png',0),
	(27,'content','default','happy-clients-w200.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w200',33674,200,95,0,1,200,0,'image/png',0),
	(28,'content','default','happy-clients-w400.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w400',107139,400,191,0,1,400,0,'image/png',0),
	(29,'content','default','happy-clients-w800.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w800',359700,800,383,0,1,800,0,'image/png',0),
	(30,'content','default','happy-clients-w1200.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w1200',760323,1200,575,0,1,1200,0,'image/png',0),
	(31,'content','default','happy-clients-w1600.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w1600',1292617,1600,767,0,1,1600,0,'image/png',0),
	(32,'content','default','happy-clients-w2000.png','',25,'png','2015-07-07 15:09:39','2015-07-07 16:09:39',0,'Happy clients w2000',1914935,2000,959,0,1,2000,0,'image/png',0),
	(33,'content','default','contact.png','orig',0,'png','2015-07-07 15:15:20','2015-07-07 16:15:20',0,'Contact',1969945,2234,1072,0,1,NULL,NULL,'image/png',0),
	(34,'content','default','contact-thumb@2x.png','thumb',33,'png','2015-07-07 15:15:20','2015-07-07 16:15:20',0,'Contact',71297,150,71,0,2,150,150,'image/png',0),
	(35,'content','default','contact-w200.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w200',34959,200,95,0,1,200,0,'image/png',0),
	(36,'content','default','contact-w400.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w400',117732,400,191,0,1,400,0,'image/png',0),
	(37,'content','default','contact-w800.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w800',390771,800,383,0,1,800,0,'image/png',0),
	(38,'content','default','contact-w1200.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w1200',800902,1200,575,0,1,1200,0,'image/png',0),
	(39,'content','default','contact-w1600.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w1600',1338008,1600,767,0,1,1600,0,'image/png',0),
	(40,'content','default','contact-w2000.png','',33,'png','2015-07-07 15:15:59','2015-07-07 16:15:59',0,'Contact w2000',1978480,2000,959,0,1,2000,0,'image/png',0);

/*!40000 ALTER TABLE `perch2_resources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_resources_to_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_resources_to_tags`;

CREATE TABLE `perch2_resources_to_tags` (
  `resourceID` int(10) NOT NULL DEFAULT '0',
  `tagID` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`resourceID`,`tagID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;



# Dump of table perch2_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_settings`;

CREATE TABLE `perch2_settings` (
  `settingID` varchar(60) NOT NULL DEFAULT '',
  `userID` int(10) unsigned NOT NULL DEFAULT '0',
  `settingValue` text NOT NULL,
  PRIMARY KEY (`settingID`,`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_settings` WRITE;
/*!40000 ALTER TABLE `perch2_settings` DISABLE KEYS */;

INSERT INTO `perch2_settings` (`settingID`, `userID`, `settingValue`)
VALUES
	('headerColour',0,'rgb(54,54,54)'),
	('content_singlePageEdit',0,'1'),
	('helpURL',0,''),
	('siteURL',0,'/'),
	('hideBranding',0,'0'),
	('content_collapseList',0,'1'),
	('lang',0,'en-gb'),
	('update_2.8.8',0,'done'),
	('headerScheme',0,'dark'),
	('update_runway_2.8.8',0,'done'),
	('latest_version',0,'2.8.8'),
	('on_sale_version',0,'2.8.8');

/*!40000 ALTER TABLE `perch2_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_user_privileges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_user_privileges`;

CREATE TABLE `perch2_user_privileges` (
  `privID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `privKey` varchar(255) NOT NULL DEFAULT '',
  `privTitle` varchar(255) NOT NULL DEFAULT '',
  `privOrder` int(10) unsigned NOT NULL DEFAULT '99',
  PRIMARY KEY (`privID`),
  UNIQUE KEY `idx_key` (`privKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_user_privileges` WRITE;
/*!40000 ALTER TABLE `perch2_user_privileges` DISABLE KEYS */;

INSERT INTO `perch2_user_privileges` (`privID`, `privKey`, `privTitle`, `privOrder`)
VALUES
	(1,'perch.login','Log in',1),
	(2,'perch.settings','Change settings',2),
	(3,'perch.users.manage','Manage users',3),
	(4,'perch.updatenotices','View update notices',4),
	(5,'content.regions.delete','Delete regions',1),
	(6,'content.regions.options','Edit region options',2),
	(7,'content.pages.edit','Edit page details',1),
	(8,'content.pages.reorder','Reorder pages',2),
	(9,'content.pages.create','Add new pages',3),
	(10,'content.pages.configure','Configure page settings',5),
	(11,'content.pages.delete','Delete pages',4),
	(12,'content.templates.delete','Delete master pages',6),
	(13,'content.navgroups.configure','Configure navigation groups',7),
	(14,'content.navgroups.create','Create navigation groups',8),
	(15,'content.navgroups.delete','Delete navigation groups',9),
	(16,'content.pages.create.toplevel','Add new top-level pages',3),
	(17,'content.pages.delete.own','Delete pages they created themselves',4),
	(18,'content.templates.configure','Configure master pages',6),
	(19,'content.pages.attributes','Edit page titles and attributes',6),
	(20,'assets.create','Upload assets',2);

/*!40000 ALTER TABLE `perch2_user_privileges` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_user_role_privileges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_user_role_privileges`;

CREATE TABLE `perch2_user_role_privileges` (
  `roleID` int(10) unsigned NOT NULL,
  `privID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`roleID`,`privID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_user_role_privileges` WRITE;
/*!40000 ALTER TABLE `perch2_user_role_privileges` DISABLE KEYS */;

INSERT INTO `perch2_user_role_privileges` (`roleID`, `privID`)
VALUES
	(1,1),
	(1,7),
	(1,8),
	(1,25),
	(2,1),
	(2,2),
	(2,3),
	(2,4),
	(2,5),
	(2,6),
	(2,7),
	(2,8),
	(2,9),
	(2,10),
	(2,11),
	(2,12);

/*!40000 ALTER TABLE `perch2_user_role_privileges` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_user_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_user_roles`;

CREATE TABLE `perch2_user_roles` (
  `roleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roleTitle` varchar(255) NOT NULL DEFAULT '',
  `roleSlug` varchar(255) NOT NULL DEFAULT '',
  `roleMasterAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`roleID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_user_roles` WRITE;
/*!40000 ALTER TABLE `perch2_user_roles` DISABLE KEYS */;

INSERT INTO `perch2_user_roles` (`roleID`, `roleTitle`, `roleSlug`, `roleMasterAdmin`)
VALUES
	(1,'Editor','editor',0),
	(2,'Admin','admin',1);

/*!40000 ALTER TABLE `perch2_user_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table perch2_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perch2_users`;

CREATE TABLE `perch2_users` (
  `userID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userUsername` varchar(255) NOT NULL DEFAULT '',
  `userPassword` varchar(255) NOT NULL DEFAULT '',
  `userCreated` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `userUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userLastLogin` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `userGivenName` varchar(255) NOT NULL DEFAULT '',
  `userFamilyName` varchar(255) NOT NULL DEFAULT '',
  `userEmail` varchar(255) NOT NULL DEFAULT '',
  `userEnabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userHash` char(32) NOT NULL DEFAULT '',
  `roleID` int(10) unsigned NOT NULL DEFAULT '1',
  `userMasterAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userID`),
  KEY `idx_enabled` (`userEnabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `perch2_users` WRITE;
/*!40000 ALTER TABLE `perch2_users` DISABLE KEYS */;

INSERT INTO `perch2_users` (`userID`, `userUsername`, `userPassword`, `userCreated`, `userUpdated`, `userLastLogin`, `userGivenName`, `userFamilyName`, `userEmail`, `userEnabled`, `userHash`, `roleID`, `userMasterAdmin`)
VALUES
	(1,'s.meehan','$P$BILhLIYvywit6Prc2MV96KkmQF3L371','2015-07-03 12:35:42','2015-07-07 16:15:59','2015-07-07 13:51:08','Stephen','Meehan','s.meehan@d3creative.uk',1,'dbcdd40a15f46875a3662c3562c79b55',2,1);

/*!40000 ALTER TABLE `perch2_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
