# Require any additional compass plugins here.
require 'breakpoint'
require "susy"


# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "perch/addons/feathers/d3feather/css"
sass_dir = "perch/addons/feathers/d3feather/sass"
images_dir = "perch/addons/feathers/d3feather/img"
javascripts_dir = "perch/addons/feathers/d3feather/js"
# fonts_dir = "perch/addons/feathers/d3feather/fonts" //doesn't appeat to be required?

output_style = :compressed
#:compressed
#:nested
# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
color_output = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass
preferred_syntax = :scss
